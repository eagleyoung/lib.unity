﻿using UnityEngine;
using UnityEngine.UI;

namespace Lib.Unity.Audio
{
    [RequireComponent(typeof(Button))]
    /// <summary>
    /// Кнопка для переключения громкости звуков
    /// </summary>
    public class SoundToggleButton : TwoStateAnimatable
    {
        /// <summary>
        /// Кнопка для реагирования
        /// </summary>
        Button button;

        /// <summary>
        /// Текущее состояние звуков
        /// </summary>
        bool SoundEnabled {
            get
            {
                return AudioHandler.SoundVolume.Float > float.Epsilon;
            }
            set
            {
                AudioHandler.SoundVolume.Float = value ? 1f : 0f;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            button = GetComponent<Button>();
            button.onClick.AddListener(Pressed);

            AudioHandler.SoundVolume.Changed += SoundChanged;
            SoundChanged();
        }

        private void SoundChanged()
        {
            Toggle(SoundEnabled);
        }

        protected virtual void OnDestroy()
        {
            if(button != null)
            {
                button.onClick.RemoveListener(Pressed);
            }
            AudioHandler.SoundVolume.Changed -= SoundChanged;
        }

        void Pressed()
        {
            SoundEnabled = !SoundEnabled;
        }
    }
}
