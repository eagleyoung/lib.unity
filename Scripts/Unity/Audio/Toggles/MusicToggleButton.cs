﻿using UnityEngine;
using UnityEngine.UI;

namespace Lib.Unity.Audio
{
    [RequireComponent(typeof(Button))]
    /// <summary>
    /// Кнопка для переключения громкости музыки
    /// </summary>
    public class MusicToggleButton : TwoStateAnimatable
    {
        /// <summary>
        /// Кнопка для реагирования
        /// </summary>
        Button button;

        /// <summary>
        /// Текущее состояние музыки
        /// </summary>
        bool MusicEnabled
        {
            get
            {
                return AudioHandler.MusicVolume.Float > float.Epsilon;
            }
            set
            {
                AudioHandler.MusicVolume.Float = value ? 1f : 0f;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            button = GetComponent<Button>();
            button.onClick.AddListener(Pressed);

            AudioHandler.MusicVolume.Changed += MusicChanged;
            MusicChanged();
        }        

        private void MusicChanged()
        {
            Toggle(MusicEnabled);
        }

        protected virtual void OnDestroy()
        {
            if (button != null)
            {
                button.onClick.RemoveListener(Pressed);
            }
            AudioHandler.MusicVolume.Changed -= MusicChanged;
        }

        void Pressed()
        {
            MusicEnabled = !MusicEnabled;
        }
    }
}
