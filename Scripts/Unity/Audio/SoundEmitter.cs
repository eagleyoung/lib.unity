﻿namespace Lib.Unity.Audio
{
    /// <summary>
    /// Объект для проигрывания определенного звука по ключу
    /// </summary>
    public class SoundEmitter : BasicObject
    {
        /// <summary>
        /// Проиграть звук
        /// </summary>
        public void PlaySound(string key)
        {
            AudioHandler.PlaySound(key);
        }
    }
}
