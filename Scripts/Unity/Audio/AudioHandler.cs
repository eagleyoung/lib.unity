﻿using Lib.Data.Param;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lib.Unity.Audio
{
    /// <summary>
    /// Класс для проигрывания музыки и звуков
    /// </summary>
    public class AudioHandler : BasicObject
    {
        static AudioHandler instance;
                
        /// <summary>
        /// Список источников звука
        /// </summary>
        List<AudioSource> soundSources = new List<AudioSource>();

        /// <summary>
        /// Источники музыки
        /// </summary>
        List<AudioSource> musicSources = new List<AudioSource>();

        /// <summary>
        /// Текущий источник звука
        /// </summary>
        int currentSound = 0;

        /// <summary>
        /// Текущий источник музыки
        /// </summary>
        int currentMusic = 0;

        [SerializeField, Tooltip("Количество источников звука")]
        /// <summary>
        /// Количество источников звука
        /// </summary>
        int soundSourceCount = 50;

        [SerializeField, Tooltip("Количество источников музыки")]
        /// <summary>
        /// Количество источников музыки
        /// </summary>
        int musicSourceCount = 2;

        protected virtual void Awake()
        {
            if(instance != null)
            {
                Destroy(gameObject);
                return;
            }
            instance = this;

            var sound = new GameObject("Sound");
            sound.transform.SetParent(transform, false);
            for (int i = 0; i < soundSourceCount; i++)
            {
                GameObject newObj = new GameObject();

                newObj.transform.SetParent(sound.transform, false);
                AudioSource source = newObj.AddComponent<AudioSource>();
                source.playOnAwake = false;
                source.volume = SoundVolume.Float;
                soundSources.Add(source);
            }

            var music = new GameObject("Music");
            music.transform.SetParent(transform, false);

            for (int i = 0; i < musicSourceCount; i++)
            {
                GameObject newObj = new GameObject();

                newObj.transform.SetParent(music.transform, false);
                AudioSource source = newObj.AddComponent<AudioSource>();
                source.playOnAwake = false;
                source.volume = MusicVolume.Float;
                musicSources.Add(source);
            }
        }

        /// <summary>
        /// Воспроизвести звук
        /// </summary>
        public static void PlaySound(string key)
        {
            if (instance == null) return;
#if DEBUG_BUILD
            Debug.Log($"[AudioHandler] Sound {key}");
#endif

            var source = instance.soundSources[instance.currentSound];
            var sound = CachedResources.Load<AudioClip>($"Audio/Sound/{key}");

            if(sound == null)
            {
#if DEBUG_BUILD
                Debug.LogError($"[AudioHandler] Отсутствует звук {key}");
#endif
                return;
            }

            source.clip = sound;
            source.Play();

            instance.currentSound++;
            if (instance.currentSound >= instance.soundSources.Count)
            {
                instance.currentSound = 0;
            }
        }

        /// <summary>
        /// Музыка играет?
        /// </summary>
        public static bool MusicPlaying { get; private set; }

        /// <summary>
        /// Воспроизвести музыку
        /// </summary>
        public static void PlayMusic(string key)
        {
            if (instance == null) return;
#if DEBUG_BUILD
            Debug.Log($"[AudioHandler] Music {key}");
#endif
            var source = instance.musicSources[instance.currentMusic];
            var music = Resources.Load<AudioClip>($"Audio/Music/{key}");

            if (music == null)
            {
#if DEBUG_BUILD
                Debug.LogError($"[AudioHandler] Отсутствует музыка {key}");
#endif
                return;
            }

            source.clip = music;            
            source.Play();

            MusicPlaying = true;
            instance.StartCoroutine(WaitMusicRoute(music.length));

            instance.currentMusic++;
            if (instance.currentMusic >= instance.musicSources.Count)
            {
                instance.currentMusic = 0;
            }
        }      
        
        static IEnumerator WaitMusicRoute(float length)
        {
            yield return new WaitForSeconds(length);
            MusicPlaying = false;
            MusicEnded();
        }

        /// <summary>
        /// Проигрываемая музыка закончилась
        /// </summary>
        public static event Action MusicEnded = delegate { };

        static Lazy<ParamData> soundVolume = new Lazy<ParamData>(()=> {
            var param = ParamManager.GetInstance().ByKey("Utility.SoundVolume", DataInterface.Core.ParamTypes.Float);
            param.Changed += ()=> {
                if(instance != null)
                {
                    foreach (var sound in instance.soundSources)
                    {
                        sound.volume = param.Float;
                    }
                }                
            };
            return param;
        });
        /// <summary>
        /// Громкость звуков
        /// </summary>
        public static ParamData SoundVolume
        {
            get
            {
                return soundVolume.Value;
            }
        }

        static Lazy<ParamData> musicVolume = new Lazy<ParamData>(()=> {
            var param = ParamManager.GetInstance().ByKey("Utility.MusicVolume", DataInterface.Core.ParamTypes.Float);
            param.Changed += () => {
                if(instance != null)
                {
                    foreach (var music in instance.musicSources)
                    {
                        music.volume = param.Float;
                    }
                }
            };
            return param;
        });
        /// <summary>
        /// Громкость музыки
        /// </summary>
        public static ParamData MusicVolume
        {
            get
            {
                return musicVolume.Value;
            }
        }
    }
}
