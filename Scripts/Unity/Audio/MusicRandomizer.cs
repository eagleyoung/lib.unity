﻿using System.Collections.Generic;
using UnityEngine;

namespace Lib.Unity.Audio
{
    /// <summary>
    /// Объект для проигрывания случайной музыки
    /// </summary>
    public class MusicRandomizer : BasicObject
    {
        [SerializeField, Tooltip("Список музыки")]
        /// <summary>
        /// Список музыки
        /// </summary>
        List<string> musicList = null;

        protected virtual void Start()
        {
            AudioHandler.MusicEnded += MusicEnded;
            MusicEnded();
        }

        private void MusicEnded()
        {
            if (!AudioHandler.MusicPlaying)
            {
                AudioHandler.PlayMusic(musicList[Random.Range(0, musicList.Count)]);
            }            
        }

        protected virtual void OnDestroy()
        {
            AudioHandler.MusicEnded -= MusicEnded;
        }
    }
}
