﻿using System;

namespace Lib.Unity.Audio
{
    /// <summary>
    /// Запрос о включении звуков и музыки
    /// </summary>
    public class AudioBlockRequest : BasicObject, IRequestable
    {
        public void GetConfirm(string idCode, bool value)
        {
            if (!value)
            {
                AudioHandler.MusicVolume.Float = 0f;
                AudioHandler.SoundVolume.Float = 0f;
            }
            Checked();
        }

        /// <summary>
        /// Проверка аудио закончена
        /// </summary>
        public static event Action Checked = delegate { };

        protected virtual void Start()
        {
            if (AudioHandler.MusicVolume.Float > 0f || AudioHandler.SoundVolume.Float > 0f)
            {
                RequestPanel.Show(this, "EnableAudioRequest");
            }
            else
            {
                Checked();
            }
        }
    }
}
