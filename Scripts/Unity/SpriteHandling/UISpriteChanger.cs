﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Lib.Unity.SpriteHandling
{
    /// <summary>
    /// Объект для смены UI изображений
    /// </summary>
    public class UISpriteChanger : BasicObject
    {
        [SerializeField, Tooltip("Список спрайтов")]
        /// <summary>
        /// Список данных
        /// </summary>
        List<Sprite> datas = null;

        [SerializeField]
        /// <summary>
        /// Изображение для смены
        /// </summary>
        Image target = null;

        /// <summary>
        /// Включить определенный спрайт
        /// </summary>
        public void SetSprite(int index)
        {
            target.sprite = datas[index];
        }
    }
}
