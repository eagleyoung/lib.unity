﻿using System.Collections.Generic;
using UnityEngine;

namespace Lib.Unity.SpriteHandling
{
    [RequireComponent(typeof(SpriteRenderer))]
    /// <summary>
    /// Объект для управления изменениями спрайтов
    /// </summary>
    public class SpriteChanger : BasicObject
    {
        [SerializeField, Tooltip("Список спрайтов")]
        /// <summary>
        /// Список данных
        /// </summary>
        List<Sprite> datas = null;

        /// <summary>
        /// Изображение для смены
        /// </summary>
        SpriteRenderer target;

        protected virtual void Awake()
        {
            target = GetComponent<SpriteRenderer>();
        }

        /// <summary>
        /// Включить определенный спрайт
        /// </summary>
        public void Toggle(int index)
        {
            target.sprite = datas[index];
        }
    }
}
