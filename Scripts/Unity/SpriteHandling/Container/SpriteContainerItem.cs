﻿using System;
using UnityEngine;

namespace Lib.Unity.SpriteHandling
{
    [Serializable]
    /// <summary>
    /// Отдельный элемент контейнера спрайтов
    /// </summary>
    public class SpriteContainerItem
    {
        /// <summary>
        /// Ключ
        /// </summary>
        public string Key;

        /// <summary>
        /// Изображение
        /// </summary>
        public Sprite Icon;
    }
}
