﻿using System.Collections.Generic;
using UnityEngine;

namespace Lib.Unity.SpriteHandling
{
    /// <summary>
    /// Контейнер спрайтов
    /// </summary>
    public class SpriteContainer : BasicObject
    {
        [SerializeField]
        /// <summary>
        /// Отдельные элементы
        /// </summary>
        List<SpriteContainerItem> items = null;

        /// <summary>
        /// Коллекция спрайтов по ключу
        /// </summary>
        static Dictionary<string, Sprite> byKey = new Dictionary<string, Sprite>();

        protected virtual void Awake()
        {
            foreach(var pack in items)
            {
                if (byKey.ContainsKey(pack.Key))
                {
#if DEBUG_BUILD
                    Debug.LogError($"[SpriteContainer] Дублирование спрайта с ключом {pack.Key}");
#endif
                    continue;
                }

                byKey.Add(pack.Key, pack.Icon);
            }
        }

        /// <summary>
        /// Получить спрайт по ключу
        /// </summary>
        public static Sprite ByKey(string key)
        {
            Sprite output = null;
            if(!byKey.TryGetValue(key, out output))
            {
#if DEBUG_BUILD
                Debug.LogError($"[SpriteContainer] Отсутствует спрайт с ключом {key}");
#endif
            }

            return output;
        }
    }
}
