﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Lib.Unity.SpriteHandling
{
    [RequireComponent(typeof(Image))]
    /// <summary>
    /// Объект для смены спрайта на случайный
    /// </summary>
    public class UISpriteRandomizer : BasicObject
    {
        /// <summary>
        /// Объект для отображения
        /// </summary>
        Image image;

        [SerializeField]
        /// <summary>
        /// Набор спрайтов
        /// </summary>
        List<Sprite> sprites = null;

        protected virtual void Awake()
        {
            image = GetComponent<Image>();
            SetRandom();
        }

        /// <summary>
        /// Установить случайный спрайт
        /// </summary>
        public void SetRandom()
        {
            if (sprites.Count == 0) return;

            image.sprite = sprites[Random.Range(0, sprites.Count)];
        }
    }
}
