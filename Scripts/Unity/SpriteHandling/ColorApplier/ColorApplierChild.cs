﻿using UnityEngine;

namespace Lib.Unity.SpriteHandling
{
    [RequireComponent(typeof(SpriteRenderer))]
    /// <summary>
    /// Объект для изменения цвета спрайта
    /// </summary>
    public class ColorApplierChild : BasicObject
    {
        SpriteRenderer sprite;
        /// <summary>
        /// Объект для отображения
        /// </summary>
        public SpriteRenderer Sprite {
            get
            {
                if(sprite == null)
                {
                    sprite = GetComponent<SpriteRenderer>();
                }
                return sprite;
            }
        }
    }
}
