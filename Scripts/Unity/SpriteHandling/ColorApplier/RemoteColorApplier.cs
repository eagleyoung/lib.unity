﻿using UnityEngine;

namespace Lib.Unity.SpriteHandling
{
    /// <summary>
    /// Объект для удаленного управления объектами для смены цвета
    /// </summary>
    public class RemoteColorApplier : BasicObject
    {
        [SerializeField]
        /// <summary>
        /// Наименование объекта - управляющего
        /// </summary>
        string remoteName = null;

        /// <summary>
        /// Объект - управляющий
        /// </summary>
        ColorApplier remote;

        protected virtual void Start()
        {
            remote = GameObject.Find(remoteName)?.GetComponent<ColorApplier>();

            if(remote != null)
            {
                foreach (var child in transform.GetComponentsInChildren<ColorApplierChild>(true))
                {
                    remote.Register(child);
                }
            }            
        }

        protected virtual void OnDestroy()
        {
            if (remote != null)
            {
                foreach (var child in transform.GetComponentsInChildren<ColorApplierChild>(true))
                {
                    remote.Unregister(child);
                }
            }
        }
    }
}
