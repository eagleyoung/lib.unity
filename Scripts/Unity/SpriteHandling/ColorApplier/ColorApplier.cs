﻿using System.Collections.Generic;
using UnityEngine;

namespace Lib.Unity.SpriteHandling
{
    /// <summary>
    /// Объект для переноса цвета на дочерние объекты
    /// </summary>
    public class ColorApplier : BasicObject
    {
        protected virtual void Awake()
        {
            foreach(var child in transform.GetComponentsInChildren<ColorApplierChild>(true))
            {
                childs.Add(child);
            }
        }

        [SerializeField]
        /// <summary>
        /// Текущий цвет
        /// </summary>
        Color current = Color.white;

        /// <summary>
        /// Последний отработанный цвет
        /// </summary>
        Color lastApplied;

        /// <summary>
        /// Дочерние объекты
        /// </summary>
        List<ColorApplierChild> childs = new List<ColorApplierChild>();

        protected virtual void Start()
        {
            Sync();
        }

        void Sync()
        {
            if(lastApplied != current)
            {
                lastApplied = current;

                foreach (var child in childs)
                {
                    child.Sprite.color = current;
                }
            }            
        }

        protected virtual void Update()
        {
            Sync();
        }

        /// <summary>
        /// Добавить дочерний компонент для отслеживания
        /// </summary>
        public void Register(ColorApplierChild child)
        {
            childs.Add(child);
        }

        /// <summary>
        /// Убрать элемент из отслеживания
        /// </summary>
        public void Unregister(ColorApplierChild child)
        {
            childs.Remove(child);
        }
    }
}
