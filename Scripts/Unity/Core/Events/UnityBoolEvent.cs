﻿using System;
using UnityEngine.Events;

namespace Lib.Unity.Events
{
    [Serializable]
    public class UnityBoolEvent : UnityEvent<bool>
    {
    }
}
