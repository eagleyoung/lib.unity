﻿using System;
using UnityEngine.Events;

namespace Lib.Unity.Events
{
    [Serializable]
    public class UnityDecimalEvent : UnityEvent<decimal>
    {
    }
}
