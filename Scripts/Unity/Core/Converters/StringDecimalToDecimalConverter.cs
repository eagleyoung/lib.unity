﻿using Lib.Unity.Events;

namespace Lib.Unity.Converters
{
    /// <summary>
    /// Конвертер string+decimal в decimal
    /// </summary>
    public class StringDecimalToDecimalConverter : BasicObject
    {
        public UnityDecimalEvent Action;

        public void Trace(string arg, decimal value)
        {
            Action.Invoke(value);
        }
    }
}
