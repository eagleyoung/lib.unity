﻿using System.Collections.Generic;
using UnityEngine;

namespace Lib.Unity
{
    /// <summary>
    /// Кэшированный вариант загрузки ресурсов
    /// </summary>
    public static class CachedResources
    {
        static Dictionary<string, Object> cache = new Dictionary<string, Object>();
        /// <summary>
        /// Загрузить ресурс
        /// </summary>
        public static T Load<T>(string path) where T : Object
        {
            Object output = null;
            if(!cache.TryGetValue(path, out output))
            {
                output = Resources.Load<T>(path);
                cache.Add(path, output);
            }
            return (T)output;
        }
    }
}
