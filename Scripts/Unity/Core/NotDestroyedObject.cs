﻿namespace Lib.Unity
{
    /// <summary>
    /// Объект, не уничтожаемый при загрузке сцены
    /// </summary>
    public class NotDestroyedObject : BasicObject
    {
        protected virtual void Awake()
        {
            DontDestroyOnLoad(this);
        }
    }
}
