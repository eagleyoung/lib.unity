﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lib.Unity
{
    [RequireComponent(typeof(Animator))]
    /// <summary>
    /// Базовый анимированный объект с различными состояниями
    /// </summary>
    public class BaseAnimatable : BasicObject
    {
        /// <summary>
        /// Аниматор для показа состояния
        /// </summary>
        protected Animator animator;

        protected virtual void Awake()
        {
            animator = GetComponent<Animator>();
            animator.keepAnimatorControllerStateOnDisable = true;
            animator.enabled = false;
        }

        /// <summary>
        /// Очередь запросов к объекту
        /// </summary>
        protected Queue<string> requests = new Queue<string>();

        /// <summary>
        /// Последний запрос
        /// </summary>
        string lastRequest;

        /// <summary>
        /// Запросить состояние объекта
        /// </summary>
        protected void Request(string state)
        {
            if (state == lastRequest) return;
            lastRequest = state;
#if DEBUG_BUILD
            Debug.Log($"[BaseAnimatable:{name}] Request {state}");
#endif
            requests.Enqueue(state);
            CheckRequest();
        }

        /// <summary>
        /// Текущий запрос
        /// </summary>
        protected string currentRequest;

        /// <summary>
        /// Проверить наличие следующего запроса
        /// </summary>
        void CheckRequest()
        {
            if (requests.Count == 0) return;
            if (currentRequest == null)
            {
                if (disable != null)
                {
                    StopCoroutine(disable);
                }
                currentRequest = requests.Dequeue();
                OnRequest(currentRequest);
            }
        }

        /// <summary>
        /// Обработать следующий запрос
        /// </summary>
        protected virtual void OnRequest(string request)
        {
#if DEBUG_BUILD
            Debug.Log($"[BaseAnimatable:{name}] ProceedRequest {request}");
#endif
            animator.enabled = true;
            animator.SetTrigger(request);
        }

        /// <summary>
        /// Послать сообщение об изменении состояния
        /// </summary>
        public void TraceState()
        {
            OnStateChange(currentRequest);
            CheckRequest();

            disable = StartCoroutine(DisableRoute());
        }

        protected virtual void OnStateChange(string state)
        {
#if DEBUG_BUILD
            Debug.Log($"[BaseAnimatable:{name}] StateChange {state}");
#endif
            currentRequest = null;
        }

        /// <summary>
        /// Время в секундах до автоматического выключения аниматора
        /// </summary>
        const float waitTillDisable = 5f;

        /// <summary>
        /// Путь выполнения выключения аниматора
        /// </summary>
        Coroutine disable;

        IEnumerator DisableRoute()
        {
            yield return new WaitForSeconds(waitTillDisable);
            if (requests.Count == 0 && currentRequest == null)
            {
                animator.enabled = false;
                OnAnimatorDisable();
            }

            disable = null;
        }

        protected virtual void OnAnimatorDisable()
        {
            
        }
    }
}
