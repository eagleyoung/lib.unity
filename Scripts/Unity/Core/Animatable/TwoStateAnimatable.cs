﻿using Lib.Unity.Events;

namespace Lib.Unity
{
    /// <summary>
    /// Анимированный объект с двумя состояниями
    /// </summary>
    public class TwoStateAnimatable : BaseAnimatable
    {
        /// <summary>
        /// Переключить состояние объекта
        /// </summary>
        public virtual void Toggle(bool isOn)
        {
            Request(isOn ? "Show" : "Hide");
        }

        protected override void OnRequest(string request)
        {
            ProceedRequest(request == "Show");
            base.OnRequest(request);            
        }

        protected virtual void ProceedRequest(bool request) { }

        protected override void OnStateChange(string state)
        {
            ProceedStateChange(state == "Show");
            base.OnStateChange(state);            
        }

        protected virtual void ProceedStateChange(bool state)
        {
            StateChanged.Invoke(state);
        }

        public UnityBoolEvent StateChanged;
    }
}
