﻿using Lib.Unity.Events;

namespace Lib.Unity
{
    /// <summary>
    /// Анимированный объект с множественными состояниями
    /// </summary>
    public class MultiStateAnimatable : BaseAnimatable
    {
        /// <summary>
        /// Переключить состояние объекта
        /// </summary>
        public void Toggle(string state)
        {
            Request(state);
        }

        protected override void OnStateChange(string state)
        {
            StateChanged.Invoke(state);
            base.OnStateChange(state);
        }

        public UnityStringEvent StateChanged;
    }
}
