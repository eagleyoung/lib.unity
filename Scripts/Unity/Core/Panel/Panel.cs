﻿using UnityEngine;

namespace Lib.Unity
{
    [RequireComponent(typeof(CanvasGroup))]
    /// <summary>
    /// Базовая панель с двумя состояниями
    /// </summary>
    public class Panel : TwoStateAnimatable
    {        
        CanvasGroup canvasGroup;

        protected override void Awake()
        {
            base.Awake();
            canvasGroup = GetComponent<CanvasGroup>();            
        }

        protected virtual void Start()
        {
            if (disablePanel && requests.Count == 0 && currentRequest == null)
            {
                gameObject.SetActive(false);
            }
        }

        [SerializeField]
        /// <summary>
        /// Выключать панель при выключенном состоянии?
        /// </summary>
        bool disablePanel = true;

        /// <summary>
        /// Обработать следующий запрос
        /// </summary>
        protected override void ProceedRequest(bool request)
        {
            if (disablePanel)
            {
                gameObject.SetActive(true);
            }
            base.ProceedRequest(request);
            canvasGroup.blocksRaycasts = true;
        }

        /// <summary>
        /// Последнее состояние панели
        /// </summary>
        bool lastState;

        protected override void ProceedStateChange(bool state)
        {
            lastState = state;
            base.ProceedStateChange(state);
            if (!state) canvasGroup.blocksRaycasts = false;
        }

        protected override void OnAnimatorDisable()
        {
            base.OnAnimatorDisable();
            if (disablePanel && !lastState)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
