﻿using UnityEngine;

namespace Lib.Unity
{
    /// <summary>
    /// Панель, при открытии которой приостанавливается время
    /// </summary>
    public class PausePanel : Panel
    {
        /// <summary>
        /// Значение времени перед паузой
        /// </summary>
        float cached;

        protected override void ProceedRequest(bool request)
        {
            base.ProceedRequest(request);
            if (request)
            {
                cached = Time.timeScale;
                Time.timeScale = 0f;
            }
            else
            {
                Time.timeScale = cached;
            }
        }
    }
}
