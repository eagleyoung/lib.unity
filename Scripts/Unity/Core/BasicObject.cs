﻿using System.Globalization;
using UnityEngine;

namespace Lib.Unity
{
    /// <summary>
    /// Базовый объект Unity
    /// </summary>
    public class BasicObject : MonoBehaviour
    {
        static BasicObject()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
        }
    }
}
