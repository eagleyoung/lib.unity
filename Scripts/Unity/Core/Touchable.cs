﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine.UI;

namespace Lib.Unity.Core
{
    /// <summary>
    /// Объект для реализации интерактивности
    /// </summary>
    public class Touchable : Text
    {
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(Touchable))]
    public class TouchableEditor : Editor {
        public override void OnInspectorGUI()
        {
            
        }
    }
#endif
}
