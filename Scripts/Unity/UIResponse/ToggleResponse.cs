﻿using UnityEngine;
using UnityEngine.UI;

namespace Lib.Unity.UIResponse
{
    [RequireComponent(typeof(Toggle))]
    /// <summary>
    /// Реагирование на нажатие переключателя
    /// </summary>
    public class ToggleResponse : BasicObject
    {
        [SerializeField, Tooltip("Данные ответа")]
        /// <summary>
        /// Данные ответа
        /// </summary>
        UIResponseData data = null;

        /// <summary>
        /// Переключатель для реагирования
        /// </summary>
        Toggle toggle;

        protected virtual void Awake()
        {
            toggle = GetComponent<Toggle>();
            toggle.onValueChanged.AddListener(Play);
        }

        void Play(bool isOn)
        {
            if (isOn)
            {
                data.Play();
            }
        }

        protected virtual void OnDestroy()
        {
            toggle.onValueChanged.RemoveListener(Play);
        }
    }
}
