﻿using UnityEngine;
using UnityEngine.UI;

namespace Lib.Unity.UIResponse
{
    [RequireComponent(typeof(Button))]
    /// <summary>
    /// Объект для воспроизведения ответа на ввод
    /// </summary>
    public class ButtonResponse : BasicObject
    {
        [SerializeField, Tooltip("Данные ответа")]
        /// <summary>
        /// Данные ответа
        /// </summary>
        UIResponseData data = null;

        /// <summary>
        /// Кнопка для реагирования
        /// </summary>
        Button button;

        protected virtual void Awake()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(Play);
        }

        void Play() {
            data.Play();
        }

        protected virtual void OnDestroy()
        {
            button.onClick.RemoveListener(Play);
        }
    }
}
