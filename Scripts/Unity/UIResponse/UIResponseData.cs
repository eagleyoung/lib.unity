﻿using System;
using UnityEngine;

namespace Lib.Unity.UIResponse
{
    [Serializable]
    /// <summary>
    /// Данные реагирования на пользовательский ввод
    /// </summary>
    public class UIResponseData
    {
        [Tooltip("Данные звука")]
        /// <summary>
        /// Ключ проигрывамого звука
        /// </summary>
        public string SoundKey;

        [Tooltip("Время вибрации в миллисекундах")]
        /// <summary>
        /// Время вибрации в миллисекундах
        /// </summary>
        public long Vibration = 50;

        /// <summary>
        /// Воспроизвести данные
        /// </summary>
        public void Play()
        {
            UIResponseManager.Play(this);
        }
    }
}
