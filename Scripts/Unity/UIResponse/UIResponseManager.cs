﻿using Lib.Unity.Audio;
using Lib.Unity.Vibration;

namespace Lib.Unity.UIResponse
{
    /// <summary>
    /// Менеджер реагирования пользовательского интерфейса
    /// </summary>
    public class UIResponseManager
    {
        /// <summary>
        /// Выполнить ответ
        /// </summary>
        public static void Play(UIResponseData data)
        {
            AudioHandler.PlaySound(data.SoundKey);
            VibrationHandler.Vibrate(data.Vibration);
        }
    }
}
