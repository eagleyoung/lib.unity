﻿using Lib.Data.Param;

namespace Lib.Unity.Param
{
    /// <summary>
    /// Объект для менеджмента данных
    /// </summary>
    public class ParamHandler : BasicObject
    {
        private void OnApplicationPause(bool pause)
        {
            if (pause)
            {
                ParamManager.GetInstance().Save();
            }
        }

        private void OnApplicationQuit()
        {
            ParamManager.GetInstance().Save();
        }
    }
}
