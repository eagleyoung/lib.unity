﻿using Lib.Data.Exp;
using Lib.Data.Loc;
using Lib.Unity.Data;
using TMPro;
using UnityEngine;

namespace Lib.Unity.Exp
{
    /// <summary>
    /// Панель показа получения опыта
    /// </summary>
    public class ExpGainedPanel : PausePanel
    {
        protected override void Awake()
        {
            base.Awake();
            ExpManager.ExpLevelChanged += LevelGained; 
        }

        [SerializeField]
        /// <summary>
        /// Текст для вывода описания
        /// </summary>
        TextMeshProUGUI description = null;

        [SerializeField]
        /// <summary>
        /// Объект для представления изменений
        /// </summary>
        AutoDataChangePresenter changes = null;

        /// <summary>
        /// Игрок получил новый уровень
        /// </summary>
        private void LevelGained()
        {
            var level = ExpManager.GetInstance().Current;
            if (level == null) return;

            if(description != null)
            {
                description.text = $"Exp.{level.Key}".Loc();
            }

            if(changes != null)
            {
                changes.Show(level.Changes);
            }

            Toggle(true);
        }

        protected override void ProceedStateChange(bool state)
        {
            base.ProceedStateChange(state);
            if (!state)
            {
                if(changes != null)
                {
                    changes.Hide();
                }
            }
        }

        protected virtual void OnDestroy()
        {
            ExpManager.ExpLevelChanged -= LevelGained;
        }
    }
}
