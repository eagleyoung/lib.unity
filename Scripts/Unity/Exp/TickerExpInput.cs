﻿using Lib.Data.Exp;
using Lib.Unity.Tickers;
using UnityEngine;

namespace Lib.Unity.Exp
{
    [RequireComponent(typeof(Ticker))]
    /// <summary>
    /// Объект для управления набором опыта
    /// </summary>
    public class TickerExpInput : BasicObject
    {
        /// <summary>
        /// Объект для показа получения опыта
        /// </summary>
        Ticker ticker;

        protected virtual void Awake()
        {
            ticker = GetComponent<Ticker>();
            ExpManager.ExpGained += Recalculate;
            ExpManager.ExpLevelChanged += Recalculate;
            Recalculate();
        }

        private void Recalculate()
        {
            var currentLevelExp = ExpManager.GetInstance().Current?.Value ?? 0;
            var nextLevelExp = ExpManager.GetInstance().Next.Value;

            var current = ExpManager.GetInstance().Exp - currentLevelExp;
            var right = nextLevelExp - currentLevelExp;

            var perc = current / right;

            ticker.Val = perc;
        }

        protected virtual void OnDestroy()
        {
            ExpManager.ExpGained -= Recalculate;
            ExpManager.ExpLevelChanged -= Recalculate;
        }
    }
}
