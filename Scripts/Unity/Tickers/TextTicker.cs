﻿using TMPro;
using UnityEngine;

namespace Lib.Unity.Tickers
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    /// <summary>
    /// Объект для визуального показа отсчета
    /// </summary>
    public class TextTicker : Ticker
    {
        /// <summary>
        /// Текст для вывода значения
        /// </summary>
        TextMeshProUGUI label;

        protected virtual void Awake()
        {
            label = GetComponent<TextMeshProUGUI>();
            current = 0;
        }

        /// <summary>
        /// Варианты показа
        /// </summary>
        public enum Variants { Default, StrRep, Percent }

        [SerializeField, Tooltip("Вариант показа")]
        /// <summary>
        /// Вариант показа
        /// </summary>
        Variants variant = Variants.Default;

        protected override double current
        {
            get
            {
                return base.current;
            }

            set
            {
                base.current = value;
                SyncValue();
            }
        }

        /// <summary>
        /// Синхронизировать показ значения
        /// </summary>
        void SyncValue()
        {
            if (label != null)
            {
                switch (variant)
                {
                    case Variants.StrRep:
                        label.text = current.StrRep();
                        break;
                    case Variants.Percent:
                        label.text = string.Format("{0:p0}", current);
                        break;
                    default:
                        label.text = current.ToString();
                        break;
                }
            }
        }
    }
}
