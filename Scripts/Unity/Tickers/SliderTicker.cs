﻿using UnityEngine;
using UnityEngine.UI;

namespace Lib.Unity.Tickers
{
    [RequireComponent(typeof(Image))]
    /// <summary>
    /// Объект для показа отсчета слайдером
    /// </summary>
    public class SliderTicker : Ticker
    {
        /// <summary>
        /// Изображение для показа
        /// </summary>
        Image slider;

        protected virtual void Awake()
        {
            slider = GetComponent<Image>();
            current = 0;
        }

        protected override double current
        {
            get
            {
                return base.current;
            }

            set
            {
                base.current = value;
                if(slider != null)
                {
                    slider.fillAmount = (float)value;
                }
            }
        }
    }
}
