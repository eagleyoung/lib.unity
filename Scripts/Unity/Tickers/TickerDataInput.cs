﻿using Lib.Data.Param;
using UnityEngine;

namespace Lib.Unity.Tickers
{
    [RequireComponent(typeof(Ticker))]
    /// <summary>
    /// Объект для отслеживания изменений данных и отправки их в объект отсчета
    /// </summary>
    public class TickerDataInput : BasicObject
    {
        /// <summary>
        /// Объект - приемник данных
        /// </summary>
        Ticker ticker;

        /// <summary>
        /// Привязанные данные
        /// </summary>
        ParamData linked;

        protected virtual void Awake()
        {
            ticker = GetComponent<Ticker>();
        }

        protected virtual void Start()
        {
            linked = ParamManager.GetInstance().ByKey(name);
            if(linked != null)
            {
                linked.Changed += LinkedChanged;
            }
            LinkedChanged();
        }

        protected virtual void OnDestroy()
        {
            if(linked != null)
            {
                linked.Changed -= LinkedChanged;
            }
        }

        private void LinkedChanged()
        {
            if(ticker != null && linked != null)
            {
                ticker.Val = linked.Double;
            }
        }
    }
}
