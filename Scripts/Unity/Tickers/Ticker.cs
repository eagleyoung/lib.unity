﻿using UnityEngine;

namespace Lib.Unity.Tickers
{
    /// <summary>
    /// Объект для визуального показа отсчета значения с плавающей запятой
    /// </summary>
    public class Ticker : BasicObject
    {
        [SerializeField, Tooltip("Максимальное время смены значения")]
        /// <summary>
        /// Максимальное время смены значения
        /// </summary>
        float timeInSeconds = 0f;

        /// <summary>
        /// Изменение значения в секунду
        /// </summary>
        double increasePerSecond;

        [SerializeField, Tooltip("Автоматически начинать отсчет")]
        /// <summary>
        /// Автоматически начинать отсчет
        /// </summary>
        bool autoStart = true;

        [SerializeField]
        /// <summary>
        /// Использовать немасштабированное время?
        /// </summary>
        bool unscaledTime = false;

        /// <summary>
        /// Обновление происходит?
        /// </summary>
        bool isTick;

        /// <summary>
        /// Начать отсчет
        /// </summary>
        public void StartTick()
        {
            isTick = true;
        }

        /// <summary>
        /// Установить значение мгновенно
        /// </summary>
        public virtual void SetInstant(double _val) {
            val = _val;
            current = val;
        }

        double val;
        /// <summary>
        /// Значение
        /// </summary>
        public virtual double Val
        {
            get
            {
                return val;
            }
            set
            {
                val = value;
                increasePerSecond = (val - current) / timeInSeconds;
                if(autoStart)
                {
                    StartTick();
                }
            }
        }

        double _current;
        /// <summary>
        /// Текущее значение
        /// </summary>
        protected virtual double current
        {
            get
            {
                return _current;
            }
            set
            {
                _current = value;
            }
        }

        protected virtual void Update()
        {
            if (isTick)
            {
                if(current == val)
                {
                    isTick = false;
                    return;
                }
                float delta = unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime;

                if (delta.Equal(0f)) return;
                current += increasePerSecond * delta;
                if((increasePerSecond > 0 && current >= Val) || (increasePerSecond < 0 && current <= Val))
                {
                    current = val;
                    isTick = false;
                }
            }
        }
    }
}
