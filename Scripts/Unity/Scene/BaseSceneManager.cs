﻿using UnityEngine;

namespace Lib.Unity.Scene
{
    /// <summary>
    /// Стандартный менеджер сцены
    /// </summary>
    public class BaseSceneManager : BasicObject
    {
        protected virtual void Awake()
        {
            Time.timeScale = 1f;
        }
    }
}
