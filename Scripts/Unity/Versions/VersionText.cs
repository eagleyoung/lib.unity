﻿using TMPro;
using UnityEngine;

namespace Lib.Unity.Versions
{
    /// <summary>
    /// Объект для показа версии игры
    /// </summary>
    [RequireComponent(typeof(TextMeshProUGUI))]    
    public class VersionText : BasicObject
    {
        protected virtual void Awake()
        {
            GetComponent<TextMeshProUGUI>().text = Application.version;
        }
    }
}
