﻿using Lib.Data.Param;
using System;
using UnityEngine;

namespace Lib.Unity.Vibration
{
    /// <summary>
    /// Объект для управления вибрацией
    /// </summary>
    public class VibrationHandler
    {
        static Lazy<ParamData> vibration = new Lazy<ParamData>(() => ParamManager.GetInstance().ByKey("Utility.Vibration", DataInterface.Core.ParamTypes.Bool));
        /// <summary>
        /// Вибрация
        /// </summary>
        public static ParamData Vibration
        {
            get
            {
                return vibration.Value;
            }
        }       

#if UNITY_ANDROID
        static AndroidJavaObject vibrator;
#endif

        static VibrationHandler()
        {
            if (Application.isEditor) Handheld.Vibrate();
#if !UNITY_EDITOR
#if UNITY_ANDROID
            vibrator = new AndroidJavaClass("com.unity3d.player.UnityPlayer")
            .GetStatic<AndroidJavaObject>("currentActivity")
            .Call<AndroidJavaObject>("getSystemService", "vibrator");
#endif
#endif
        }

        public static void Vibrate(long milliseconds)
        {
            if (!Vibration.Bool) return;
#if !UNITY_EDITOR
#if UNITY_ANDROID
            vibrator.Call("vibrate", milliseconds);
#endif
#endif
        }

        public static void Vibrate(long[] pattern, int repeat)
        {
            if (!Vibration.Bool) return;
#if !UNITY_EDITOR
#if UNITY_ANDROID
            vibrator.Call("vibrate", pattern, repeat);
#endif
#endif
        }
    }
}
