﻿using UnityEngine;
using UnityEngine.UI;

namespace Lib.Unity.Vibration
{
    [RequireComponent(typeof(Button))]
    /// <summary>
    /// Кнопка для переключения вибрации
    /// </summary>
    public class VibrationToggleButton : TwoStateAnimatable
    {
        /// <summary>
        /// Кнопка для реагирования
        /// </summary>
        Button button;

        /// <summary>
        /// Текущее состояние вибрации
        /// </summary>
        bool VibrationEnabled
        {
            get
            {
                return VibrationHandler.Vibration.Bool;
            }
            set
            {
                VibrationHandler.Vibration.Bool = value;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            button = GetComponent<Button>();
            button.onClick.AddListener(Pressed);

            VibrationHandler.Vibration.Changed += VibrationChanged;
            VibrationChanged();
        }

        private void VibrationChanged()
        {
            Toggle(VibrationEnabled);
        }

        protected virtual void OnDestroy()
        {
            if (button != null)
            {
                button.onClick.RemoveListener(Pressed);
            }
            VibrationHandler.Vibration.Changed -= VibrationChanged;
        }

        void Pressed()
        {
            VibrationEnabled = !VibrationEnabled;
        }
    }
}
