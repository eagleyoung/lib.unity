﻿using UnityEngine;
using UnityEngine.UI;

namespace Lib.Unity.WWW
{
    [RequireComponent(typeof(Button))]
    /// <summary>
    /// Кнопка для открытия ссылки
    /// </summary>
    public class BrowserButton : BasicObject
    {
        [SerializeField, Tooltip("Путь для открытия в браузере")]
        /// <summary>
        /// Путь для открытия в браузере
        /// </summary>
        string URL = null;

        protected virtual void Start()
        {
            GetComponent<Button>().onClick.AddListener(OpenUrl);
        }

        /// <summary>
        /// Открыть страницу в браузере
        /// </summary>
        protected virtual void OpenUrl()
        {
            Application.OpenURL(URL);
        }

        protected virtual void OnDestroy()
        {
            GetComponent<Button>().onClick.RemoveListener(OpenUrl);
        }
    }
}
