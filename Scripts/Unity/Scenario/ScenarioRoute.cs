﻿using Lib.Data.Param;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Lib.Unity.Scenario
{
    [RequireComponent(typeof(Button), typeof(Animator))]
    /// <summary>
    /// Менеджер управления сценарием
    /// </summary>
    public class ScenarioRoute : BasicObject
    {
        /// <summary>
        /// Событие, вызываемое при закрытии сценария
        /// </summary>
        public static event Action<string> ScenarioEnded = delegate { };
        static void OnScenarioEnded(string key)
        {
            var chk = ParamManager.GetInstance().ByKey(key, DataInterface.Core.ParamTypes.Bool);
            chk.Bool = true;

            ScenarioEnded?.Invoke(key);
        }

        /// <summary>
        /// Выгрузить сцену
        /// </summary>
        public void Unload()
        {
            OnScenarioEnded(gameObject.scene.name);
            SceneManager.UnloadSceneAsync(gameObject.scene);            
        }

        /// <summary>
        /// Кнопка для реагирования
        /// </summary>
        Button button;

        /// <summary>
        /// Аниматор
        /// </summary>
        Animator animator;

        protected virtual void Awake()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(Click);

            animator = GetComponent<Animator>();
        }

        void Click()
        {
            animator.SetTrigger("ToggleClose");
        }

        protected virtual void OnDestroy()
        {
            button.onClick.RemoveListener(Click);
        }
    }
}
