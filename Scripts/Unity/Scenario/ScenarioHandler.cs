﻿using Lib.Data.Param;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Lib.Unity.Scenario
{
    /// <summary>
    /// Объект для управления сценариями
    /// </summary>
    public class ScenarioHandler : BasicObject
    {
        static ScenarioHandler instance;
        protected virtual void Awake()
        {
            if(instance != null)
            {
                Destroy(gameObject);
                return;
            }
            instance = this;
        }

        protected virtual void OnDestroy()
        {
            if(instance == this)
            {
                instance = null;
            }
        }

        /// <summary>
        /// Идентификатор сцены для загрузки
        /// </summary>
        static string toShow;

        /// <summary>
        /// Показать сценарий по определенному ключу. 
        /// При повторном запросе показ не производится.
        /// </summary>
        public static void Show(string key)
        {
            if (instance == null) return;

            toShow = $"Scenario_{key}";
#if DEBUG_BUILD
            Debug.Log($"[ScenarioHandler] Запрос сценария {toShow}");
#endif
            var chk = ParamManager.GetInstance().ByKey(toShow, DataInterface.Core.ParamTypes.Bool);
            if (!chk.Bool)
            {
                if (!Application.CanStreamedLevelBeLoaded(toShow))
                {
#if DEBUG_BUILD
                    Debug.Log("Запрашиваемая сцена отсутствует");
#endif
                    return;
                }

                SceneManager.LoadScene(toShow, LoadSceneMode.Additive);
            }
        }
    }
}
