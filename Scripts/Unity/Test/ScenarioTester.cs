﻿using Lib.Data.Param;
using Lib.Unity.Scenario;

namespace Lib.Unity.Test
{
    /// <summary>
    /// Объект для тестирования сценария
    /// </summary>
    public class ScenarioTester : BasicObject
    {
        /// <summary>
        /// Запустить показ сценария
        /// </summary>
        public void Toggle(string key)
        {
            var toShow = $"Scenario_{key}";
            ParamManager.GetInstance().ByKey(toShow, DataInterface.Core.ParamTypes.Bool).Bool = false;
            ScenarioHandler.Show(key);
        }
    }
}
