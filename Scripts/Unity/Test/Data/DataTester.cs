﻿using Lib.Data.Param;
using System.Collections.Generic;
using UnityEngine;

namespace Lib.Unity.Test
{
    /// <summary>
    /// Объект для проведения тестирования данных
    /// </summary>
    public class DataTester : BasicObject
    {
        [SerializeField]
        /// <summary>
        /// Данные для тестов
        /// </summary>
        List<DataTesterItem> tests = null;

        protected virtual void Update()
        {
            foreach(var test in tests)
            {
                if (Input.GetKeyDown(test.Key))
                {
                    var param = ParamManager.GetInstance().ByKey(test.ParamKey);

                    if(param == null)
                    {
#if DEBUG_BUILD
                        Debug.LogError($"[DataTester] Отсутствует параметр с идентификатором {test.Key}");
#endif
                        continue;
                    }

                    param += test.Change;
                }
            }
        }
    }
}
