﻿using System;
using UnityEngine;

namespace Lib.Unity.Test
{
    [Serializable]
    /// <summary>
    /// Отдельный элемент теста
    /// </summary>
    public class DataTesterItem
    {
        /// <summary>
        /// Ключ кнопки
        /// </summary>
        public KeyCode Key;

        /// <summary>
        /// Ключ параметра для изменения
        /// </summary>
        public string ParamKey;

        /// <summary>
        /// Изменение параметра
        /// </summary>
        public string Change;
    }
}
