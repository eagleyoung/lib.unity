﻿using Lib.Data.Exp;
using UnityEngine;

namespace Lib.Unity.Test
{
    /// <summary>
    /// Объект для тестирования системы опыта
    /// </summary>
    public class ExpTest : KeyTesterModule
    {
        [SerializeField]
        /// <summary>
        /// Значение изменения параметра опыта
        /// </summary>
        double addExpValue = 0d;

        protected override void Registration(KeyTester tester)
        {
            tester.Register(UnityEngine.KeyCode.E, () => {
                ExpManager.GetInstance().Exp += addExpValue;
            });
        }
    }
}
