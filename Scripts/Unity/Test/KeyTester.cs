﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Lib.Unity.Test
{
    /// <summary>
    /// Объект для проведения тестирования элементов
    /// </summary>
    public class KeyTester : BasicObject
    {
        /// <summary>
        /// Список тестов
        /// </summary>
        Dictionary<KeyCode, Action> actions = new Dictionary<KeyCode, Action>();

        /// <summary>
        /// Зарегистрировать новый тест
        /// </summary>
        public void Register(KeyCode key, Action action)
        {
            if (actions.ContainsKey(key))
            {
#if DEBUG_BUILD
                Debug.LogError($"[KeyTester] Дублирование ключа {key}");
#endif
                return;
            }

            actions.Add(key, action);
        }

        protected virtual void Update()
        {
            foreach(var item in actions)
            {
                if (Input.GetKeyDown(item.Key))
                {
                    item.Value.Invoke();
                }
            }
        }
    }
}
