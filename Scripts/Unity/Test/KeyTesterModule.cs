﻿using UnityEngine;

namespace Lib.Unity.Test
{
    [RequireComponent(typeof(KeyTester))]
    /// <summary>
    /// Модуль для отдельного тестирования с использованием объекта KeyTester
    /// </summary>
    public abstract class KeyTesterModule : BasicObject
    {
        protected virtual void Awake()
        {
            var tester = GetComponent<KeyTester>();
            Registration(tester);
        }

        /// <summary>
        /// Регистрация отдельных тестов
        /// </summary>
        protected abstract void Registration(KeyTester tester);
    }
}
