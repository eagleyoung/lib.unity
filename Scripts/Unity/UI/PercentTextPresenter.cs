﻿using TMPro;
using UnityEngine;

namespace Lib.Unity.UI
{
    /// <summary>
    /// Представление процентов в текстовом виде
    /// </summary>
    public class PercentTextPresenter : BasicObject
    {
        [SerializeField]
        TextMeshProUGUI output = null;

        /// <summary>
        /// Установить значение
        /// </summary>
        public void Set(decimal percent)
        {
            output.text = string.Format("{0:p0}", percent);
        }
    }
}
