﻿using System.Collections.Generic;
using UnityEngine;

namespace Lib.Unity.UI
{
    /// <summary>
    /// Набор панелей в возможностью переключения
    /// </summary>
    public class SelectionPanel : BasicObject
    {
        /// <summary>
        /// Список доступных панелей по наименованию
        /// </summary>
        Dictionary<string, GameObject> panels = new Dictionary<string, GameObject>();

        /// <summary>
        /// Выбранная панель
        /// </summary>
        GameObject selected;

        protected virtual void Awake()
        {
            for(int i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i);
                panels.Add(child.name, child.gameObject);
                child.gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// Выбрать панель
        /// </summary>
        public void Select(string name)
        {
            if(selected == null || selected.name != name)
            {
                if(selected != null)
                {
                    selected.SetActive(false);
                }
                
                GameObject toSelect = null;
                if(!panels.TryGetValue(name, out toSelect))
                {
                    Debug.LogErrorFormat("[SelectionPanel] Отсутствует панель {0}", name);
                    selected = null;
                    return;
                }
                selected = toSelect;
                selected.SetActive(true);
            }
        }
    }
}
