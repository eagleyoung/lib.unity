﻿using Lib.Unity.Events;
using UnityEngine.UI;

namespace Lib.Unity.UI
{
    /// <summary>
    /// Набор переключателей с рассылкой информации о включенном ключе
    /// </summary>
    public class ExtendedToggleGroup : ToggleGroup
    {
        protected override void Awake()
        {
            base.Awake();
            foreach(var checkBox in transform.GetComponentsInChildren<Toggle>(true))
            {
                checkBox.onValueChanged.AddListener((selected) =>
                {
                    if (selected)
                    {
                        OnSelected(checkBox);
                    }
                });
            }
        }

        /// <summary>
        /// Возможность реагирования элементов группы
        /// </summary>
        public bool Interactable
        {
            set
            {
                foreach (var checkBox in transform.GetComponentsInChildren<Toggle>(true))
                {
                    checkBox.interactable = value;
                }
            }
        }

        /// <summary>
        /// Выбрать определенный переключатель
        /// </summary>
        public void Select(string name)
        {
            foreach (var checkBox in transform.GetComponentsInChildren<Toggle>(true))
            {
                if(checkBox.name == name)
                {
                    checkBox.isOn = true;
                }
            }
        }

        protected override void Start()
        {
            base.Start();
            foreach (var toggle in transform.GetComponentsInChildren<Toggle>(true))
            {                
                if (toggle.isOn) OnSelected(toggle);
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            foreach (var checkBox in transform.GetComponentsInChildren<Toggle>(true))
            {
                checkBox.onValueChanged.RemoveAllListeners();
            }
        }

        public UnityStringEvent Selected;
        protected virtual void OnSelected(Toggle toggle)
        {
            Selected.Invoke(toggle.name);
        }
    }
}
