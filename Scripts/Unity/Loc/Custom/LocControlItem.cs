﻿using System;
using UnityEngine;

namespace Lib.Unity.Loc
{
    [Serializable]
    /// <summary>
    /// Описание отображения языка
    /// </summary>
    public class LocControlItem
    {
        /// <summary>
        /// Ключ языка
        /// </summary>
        public string Key;

        /// <summary>
        /// Иконка языка
        /// </summary>
        public Sprite Icon;
    }
}
