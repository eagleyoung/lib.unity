﻿using Lib.Data.Loc;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Lib.Unity.Loc
{
    /// <summary>
    /// Объект для переключения языка
    /// </summary>
    public class LocControl : BasicObject
    {
        [SerializeField]
        /// <summary>
        /// Кнопка для переключения вверх
        /// </summary>
        Button up = null;

        [SerializeField]
        /// <summary>
        /// Кнопка для переключения вниз
        /// </summary>
        Button down = null;

        [SerializeField]
        /// <summary>
        /// Иконка для показа языка
        /// </summary>
        Image icon = null;

        [SerializeField]
        /// <summary>
        /// Список доступных языков
        /// </summary>
        List<LocControlItem> langs = null;

        protected virtual void Awake()
        {
            up.onClick.AddListener(Up);
            down.onClick.AddListener(Down);
            LocManager.GetInstance().CurrentLang.Changed += LocChanged;
            LocChanged();
        }

        /// <summary>
        /// Переключить язык наверх
        /// </summary>
        void Up() {
            var previousIndex = currentItem - 1;
            if (previousIndex < 0) previousIndex = langs.Count - 1;

            LocManager.GetInstance().CurrentLang.String = langs[previousIndex].Key;
        }

        /// <summary>
        /// Переключить язык вниз
        /// </summary>
        void Down() {
            var nextIndex = currentItem + 1;
            if (nextIndex >= langs.Count) nextIndex = 0;

            LocManager.GetInstance().CurrentLang.String = langs[nextIndex].Key;
        }

        /// <summary>
        /// Текущий выбор
        /// </summary>
        int currentItem;

        private void LocChanged()
        {
            var currentLang = LocManager.GetInstance().CurrentLang.String;
            foreach (var langPack in langs)
            {
                if(langPack.Key == currentLang)
                {
                    currentItem = langs.IndexOf(langPack);
                    icon.sprite = langPack.Icon;
                    return;
                }
            }
#if DEBUG_BUILD
            Debug.LogError($"[LocControl] Отсутствует набор для показа языка {currentLang}");
#endif
        }

        protected virtual void OnDestroy()
        {
            up.onClick.RemoveListener(Up);
            down.onClick.RemoveListener(Down);
            LocManager.GetInstance().CurrentLang.Changed -= LocChanged;
        }
    }
}
