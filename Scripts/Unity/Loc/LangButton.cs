﻿using Lib.Data.Loc;
using UnityEngine;
using UnityEngine.UI;

namespace Lib.Unity.Loc
{
    [RequireComponent(typeof(Button))]
    /// <summary>
    /// Кнопка выбора языка
    /// </summary>
    public class LangButton : TwoStateAnimatable
    {
        /// <summary>
        /// Объект взаимодействия
        /// </summary>
        Button button;
        
        protected override void Awake()
        {
            base.Awake();
            button = GetComponent<Button>();
            button.onClick.AddListener(ChangeLang);
            LocManager.GetInstance().CurrentLang.Changed += LocChanged;
            LocChanged();
        }

        private void LocChanged()
        {
            Toggle(LocManager.GetInstance().CurrentLang.String == name);
        }

        /// <summary>
        /// Изменить язык
        /// </summary>
        void ChangeLang()
        {
            LocManager.GetInstance().CurrentLang.String = name;
        }

        protected virtual void OnDestroy()
        {
            if(button != null)
            {
                button.onClick.RemoveListener(ChangeLang);
            }            
            LocManager.GetInstance().CurrentLang.Changed -= LocChanged;
        }
    }
}
