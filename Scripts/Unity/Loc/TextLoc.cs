﻿using Lib.Data.Loc;
using TMPro;
using UnityEngine;

namespace Lib.Unity.Loc
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    /// <summary>
    /// Компонент для автоматического перевода текста
    /// </summary>
    public class TextLoc : BasicObject
    {
        /// <summary>
        /// Объект для вывода текста
        /// </summary>
        TextMeshProUGUI textObject;

        /// <summary>
        /// Ключ текста
        /// </summary>
        string key;

        protected virtual void Awake()
        {
            textObject = GetComponent<TextMeshProUGUI>();
            if(textObject != null)
            {
                key = textObject.text;
            }            
            LocManager.GetInstance().CurrentLang.Changed += UpdateText;
            UpdateText();
        }

        private void UpdateText()
        {
            if(textObject != null)
            {
                textObject.text = key.Loc();
            }
        }

        protected virtual void OnDestroy()
        {
            LocManager.GetInstance().CurrentLang.Changed -= UpdateText;
        }
    }
}
