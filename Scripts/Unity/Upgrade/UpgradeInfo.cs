﻿using Lib.Data.Loc;
using Lib.Data.Upgrade;
using TMPro;
using UnityEngine;

namespace Lib.Unity.Upgrade
{
    /// <summary>
    /// Объект для показа информации об улучшении
    /// </summary>
    public class UpgradeInfo : BasicObject
    {
        [SerializeField, Tooltip("Объект для показа начальной помощи")]
        /// <summary>
        /// Объект для показа начальной помощи
        /// </summary>
        GameObject helpInfo = null;

        [SerializeField, Tooltip("Объект для показа основного описания")]
        /// <summary>
        /// Объект для показа основного описания
        /// </summary>
        GameObject mainInfo = null;

        [SerializeField, Tooltip("Текст для вывода названия")]
        /// <summary>
        /// Текст для вывода названия
        /// </summary>
        TextMeshProUGUI label = null;

        [SerializeField, Tooltip("Текст для вывода описания")]
        /// <summary>
        /// Текст для вывода описания
        /// </summary>
        TextMeshProUGUI description = null;

        protected virtual void Awake()
        {
            UpgradeButton.Selected += UpgradeSelected;
        }

        protected virtual void Start()
        {
            Hide();
        }

        /// <summary>
        /// Скрыть информацию
        /// </summary>
        public void Hide()
        {
            helpInfo.SetActive(true);
            mainInfo.SetActive(false);
        }

        protected virtual void OnDestroy()
        {
            UpgradeButton.Selected -= UpgradeSelected;
        }

        private void UpgradeSelected(UpgradeData obj)
        {
            helpInfo.SetActive(false);
            mainInfo.SetActive(true);
            label.text = $"Upgrade.{obj.Key}".Loc();
            description.text = $"Upgrade.{obj.Key}.Description".Loc();
        }
    }
}
