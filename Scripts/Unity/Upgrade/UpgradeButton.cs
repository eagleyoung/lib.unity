﻿using Lib.Data.Upgrade;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Lib.Unity.Upgrade
{
    [RequireComponent(typeof(Button))]
    /// <summary>
    /// Кнопка показа улучшения
    /// </summary>
    public class UpgradeButton : MultiStateAnimatable
    {
        /// <summary>
        /// Привязанное улучшение
        /// </summary>
        UpgradeData linked;

        Button button;

        protected override void Awake()
        {
            base.Awake();
            button = GetComponent<Button>();
            button.onClick.AddListener(Show);
        }

        protected virtual void Start()
        {
            linked = UpgradeManager.GetInstance().ByKey(name);
            linked.StateChanged += Check;
            Check();
        }

        private void Check()
        {
            string selectedState = null;

            if (linked.Bought && !linked.Repeatable)
            {
                selectedState = "Bought";
            }
            else
            {
                selectedState = linked.Requires.Available ? "Active" : "Disabled";
            }

            Toggle(selectedState);
        }

        protected virtual void OnDestroy()
        {
            if (button != null) button.onClick.RemoveListener(Show);
            if(linked != null) linked.StateChanged -= Check;
        }

        /// <summary>
        /// Улучшение было выбрано
        /// </summary>
        public static event Action<UpgradeData> Selected = delegate { };
        static void OnSelected(UpgradeData data)
        {
            Selected?.Invoke(data);
        }

        /// <summary>
        /// Выбранная кнопка
        /// </summary>
        static UpgradeButton selected;

        [SerializeField]
        /// <summary>
        /// Объект для подсветки выбора
        /// </summary>
        TwoStateAnimatable highlight = null;

        /// <summary>
        /// Показать улучшение
        /// </summary>
        void Show()
        {
            if (selected == this) return;

            if(selected != null)
            {
                selected.highlight?.Toggle(false);
            }

            selected = this;
            highlight?.Toggle(true);

            OnSelected(linked);
        }
    }
}
