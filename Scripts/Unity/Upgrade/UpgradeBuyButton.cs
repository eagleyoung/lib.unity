﻿using Lib.Data.Upgrade;
using Lib.Unity.Data;
using UnityEngine;
using UnityEngine.UI;

namespace Lib.Unity.Upgrade
{
    [RequireComponent(typeof(Button))]
    /// <summary>
    /// Кнопка для покупки улучшения
    /// </summary>
    public class UpgradeBuyButton : BasicObject
    {
        /// <summary>
        /// Объект для реагирования
        /// </summary>
        Button button;

        protected virtual void Awake()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(Buy);
            UpgradeButton.Selected += UpgradeSelected;
            button.interactable = false;
        }

        protected virtual void OnDestroy()
        {
            UpgradeButton.Selected -= UpgradeSelected;
            if(button != null)
            {
                button.onClick.RemoveListener(Buy);
            }
        }

        /// <summary>
        /// Выбранное улучшение
        /// </summary>
        UpgradeData selected;

        [SerializeField]
        /// <summary>
        /// Объект для показа требований
        /// </summary>
        BaseDataRequirePresenter requirePresenter = null;

        private void UpgradeSelected(UpgradeData obj)
        {
            selected = obj;
            var upgradeEnabled = !obj.Bought || obj.Repeatable;

            button.interactable = upgradeEnabled && obj.Requires.Available;
            
            if (requirePresenter != null)
            {
                if (upgradeEnabled)
                {
                    requirePresenter.Show(obj.Requires);
                }
                else
                {
                    requirePresenter.Hide();
                }                
            }
        }

        /// <summary>
        /// Купить улучшение
        /// </summary>
        void Buy()
        {
            selected.Bought = true;
            selected.Requires.Proceed();
            selected.Changes.Proceed();

            UpgradeSelected(selected);
        }
    }
}
