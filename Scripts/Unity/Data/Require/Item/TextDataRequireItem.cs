﻿using Lib.Data;
using TMPro;
using UnityEngine;

namespace Lib.Unity.Data
{
    /// <summary>
    /// Отдельный объект для показа требования
    /// </summary>
    public class TextDataRequireItem : BaseDataRequireItem
    {
        [SerializeField]
        /// <summary>
        /// Объект для вывода текста
        /// </summary>
        TextMeshProUGUI label = null;

        /// <summary>
        /// Показать требование
        /// </summary>
        public override void Show(DataRequire require) {
            gameObject.SetActive(true);

            label.text = require.StrRep();
        }
    }
}
