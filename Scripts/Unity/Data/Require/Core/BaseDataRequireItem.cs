﻿using Lib.Data;

namespace Lib.Unity.Data
{
    /// <summary>
    /// Базовый объект для показа требований
    /// </summary>
    public abstract class BaseDataRequireItem : BasicObject
    {
        public abstract void Show(DataRequire require);
    }
}
