﻿using Lib.Data;

namespace Lib.Unity.Data
{
    /// <summary>
    /// Объект для представления набора требований
    /// </summary>
    public abstract class BaseDataRequirePresenter : BasicObject
    {
        /// <summary>
        /// Загрузить набор данных для представления
        /// </summary>
        public abstract void Show(DataRequireCollection requires);

        /// <summary>
        /// Скрыть представление
        /// </summary>
        public abstract void Hide();
    }
}
