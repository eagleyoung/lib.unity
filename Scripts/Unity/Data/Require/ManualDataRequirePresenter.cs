﻿using System.Collections.Generic;
using System.Linq;
using Lib.Data;
using UnityEngine;

namespace Lib.Unity.Data
{
    /// <summary>
    /// Объект для ручной настройки показа требований
    /// </summary>
    public class ManualDataRequirePresenter : BaseDataRequirePresenter
    {
        /// <summary>
        /// Доступные элементы для вывода
        /// </summary>
        Dictionary<string, BaseDataRequireItem> items = new Dictionary<string, BaseDataRequireItem>();

        protected virtual void Awake()
        {
            foreach(var child in GetComponentsInChildren<TextDataRequireItem>(true))
            {
                items.Add(child.name, child);
                child.gameObject.SetActive(false);
            }
        }

        public override void Show(DataRequireCollection requires)
        {
            List<BaseDataRequireItem> toHide = items.Values.ToList();
            foreach(var require in requires)
            {
                BaseDataRequireItem output = null;
                if(!items.TryGetValue(require.ParamKey, out output))
                {
#if DEBUG_BUILD
                    Debug.LogWarning($"[ManualDataRequirePresenter:{name}] Отсутствует объект для показа требования {require.ParamKey}");
                    continue;
#endif
                }

                output.Show(require);

                if (toHide.Contains(output))
                {
                    toHide.Remove(output);
                }
            }

            foreach(var item in toHide)
            {
                item.gameObject.SetActive(false);
            }
        }

        public override void Hide()
        {
            foreach(var item in items.Values)
            {
                item.gameObject.SetActive(false);
            }
        }
    }
}
