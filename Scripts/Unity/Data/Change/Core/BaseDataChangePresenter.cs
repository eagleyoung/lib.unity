﻿using Lib.Data;

namespace Lib.Unity.Data
{
    /// <summary>
    /// Объект для представления набора изменений
    /// </summary>
    public abstract class BaseDataChangePresenter : BasicObject
    {
        /// <summary>
        /// Загрузить набор данных для представления
        /// </summary>
        public abstract void Show(DataChangeCollection changes);

        /// <summary>
        /// Скрыть представление
        /// </summary>
        public abstract void Hide();
    }
}
