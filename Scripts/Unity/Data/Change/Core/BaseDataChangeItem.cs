﻿using Lib.Data;

namespace Lib.Unity.Data
{
    /// <summary>
    /// Базовый объект для показа изменений параметров
    /// </summary>
    public abstract class BaseDataChangeItem : BasicObject
    {
        /// <summary>
        /// Показать изменение
        /// </summary>
        public abstract void Show(DataChange change);
    }
}
