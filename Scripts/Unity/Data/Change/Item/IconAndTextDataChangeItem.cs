﻿using Lib.Data;
using Lib.Unity.SpriteHandling;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Lib.Unity.Data
{
    /// <summary>
    /// Объект для показа отдельного изменения
    /// </summary>
    public class IconAndTextDataChangeItem : BaseDataChangeItem
    {
        [SerializeField]
        /// <summary>
        /// Объект для вывода изменения
        /// </summary>
        TextMeshProUGUI label = null;

        [SerializeField]
        /// <summary>
        /// Изображение для вывода
        /// </summary>
        Image icon = null;

        /// <summary>
        /// Показать изменение
        /// </summary>
        public override void Show(DataChange change)
        {
            if(label != null)
            {
                label.text = change.StrRep();
            }

            if(icon != null)
            {
                icon.sprite = SpriteContainer.ByKey(change.ParamKey);
            }
        }
    }
}
