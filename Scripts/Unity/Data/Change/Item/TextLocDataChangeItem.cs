﻿using Lib.Data;
using Lib.Data.Loc;
using TMPro;
using UnityEngine;

namespace Lib.Unity.Data
{
    /// <summary>
    /// Объект для создания текстового варианта отображения
    /// </summary>
    public class TextLocDataChangeItem : BaseDataChangeItem
    {
        [SerializeField]
        /// <summary>
        /// Объект для вывода изменения
        /// </summary>
        TextMeshProUGUI label = null;

        /// <summary>
        /// Показать изменение
        /// </summary>
        public override void Show(DataChange change)
        {
            if (label != null)
            {
                label.text = $"{change.StrRep()} {change.ParamKey.Loc()}";
            }
        }
    }
}
