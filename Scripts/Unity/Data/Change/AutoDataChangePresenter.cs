﻿using Lib.Data;
using UnityEngine;

namespace Lib.Unity.Data
{
    /// <summary>
    /// Объект для автоматического показа изменений параметров
    /// </summary>
    public class AutoDataChangePresenter : BaseDataChangePresenter
    {
        [SerializeField]
        /// <summary>
        /// Префаб для показа отдельного изменения
        /// </summary>
        BaseDataChangeItem prefab = null;

        /// <summary>
        /// Показать набор изменений
        /// </summary>
        public override void Show(DataChangeCollection changes)
        {
            foreach(var change in changes)
            {
                var inst = Instantiate(prefab);
                inst.Show(change);
                inst.transform.SetParent(transform, false);
            }
        }

        /// <summary>
        /// Скрыть представление
        /// </summary>
        public override void Hide()
        {
            foreach (Transform child in transform)
            {
                Destroy(child.gameObject);
            }
        }
    }
}
