﻿using UnityEngine;
using UnityEngine.UI;

namespace Lib.Unity.Utility
{
    [RequireComponent(typeof(Button))]
    /// <summary>
    /// Кнопка загрузки сцены
    /// </summary>
    public class RequestSceneButton : BasicObject, IRequestable
    {
        [SerializeField, Tooltip("Строка запроса")]
        /// <summary>
        /// Строка запроса
        /// </summary>
        string request = null;

        [SerializeField, Tooltip("Ключ сцены для загрузки")]
        /// <summary>
        /// Ключ сцены для загрузки
        /// </summary>
        string sceneKey = null;

        Button button;

        protected virtual void Awake()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(RequestScene);
        }

        protected virtual void OnDestroy()
        {
            if(button != null)
            {
                button.onClick.RemoveListener(RequestScene);
            }
        }

        /// <summary>
        /// Запросить загрузку сцены
        /// </summary>
        void RequestScene()
        {
            if (string.IsNullOrEmpty(request))
            {
                LoadHandler.RequestScene(sceneKey);
            }
            else
            {
                RequestPanel.Show(this, request);
            }
        }

        /// <summary>
        /// Игрок ответил на вопрос
        /// </summary>
        public void GetConfirm(string idCode, bool value)
        {
            if(value) {
                LoadHandler.RequestScene(sceneKey);
            }
        } 
    }
}
