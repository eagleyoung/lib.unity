﻿using UnityEngine;

namespace Lib.Unity
{
    /// <summary>
    /// Объект для отключения затухания экрана
    /// </summary>
    public class ScreenLocker : BasicObject
    {
        protected virtual void Awake()
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }
    }
}
