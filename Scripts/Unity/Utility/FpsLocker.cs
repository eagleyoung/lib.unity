﻿using UnityEngine;

namespace Lib.Unity
{
    /// <summary>
    /// Объект для фиксирования FPS
    /// </summary>
    public class FpsLocker : BasicObject
    {
        [SerializeField, Tooltip("Требуемое количество кадров в секунду")]
        /// <summary>
        /// Требуемое количество кадров в секунду
        /// </summary>
        int targetFps = 60;

        protected virtual void Awake()
        {
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = targetFps;
        }
    }
}
