﻿using UnityEngine;
using UnityEngine.UI;

namespace Lib.Unity.Utility
{
    [RequireComponent(typeof(Button))]
    /// <summary>
    /// Кнопка выхода
    /// </summary>
    public class RequestQuitButton : BasicObject, IQuitResponsible, IRequestable
    {
        [SerializeField, Tooltip("Строка запроса")]
        /// <summary>
        /// Строка запроса
        /// </summary>
        string request = null;

        Button button;

        protected virtual void Awake()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(RequestQuit);

            QuitHandler.Register(this);
        }

        protected virtual void OnDestroy()
        {
            QuitHandler.Unregister(this);
            if(button != null)
            {
                button.onClick.RemoveListener(RequestQuit);
            }
        }

        /// <summary>
        /// Игрок нажал кнопку "Назад"
        /// </summary>
        public void QuitPressed()
        {
            RequestQuit();
        }

        /// <summary>
        /// Запросить выход
        /// </summary>
        void RequestQuit()
        {
            if (string.IsNullOrEmpty(request))
            {
                LoadHandler.RequestQuit();
            }
            else
            {
                RequestPanel.Show(this, "QuitGameRequest");
            }            
        }

        /// <summary>
        /// Игрок ответил на вопрос
        /// </summary>
        public void GetConfirm(string idCode, bool value)
        {
            if(value) {
                LoadHandler.RequestQuit();
            }
        } 
    }
}
