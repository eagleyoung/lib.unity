﻿using Lib.Data.Loc;
using TMPro;
using UnityEngine;

namespace Lib.Unity
{
    /// <summary>
    /// Панель с вопросом и двумя вариантами ответа
    /// </summary>
    public class RequestPanel : PausePanel
    {
        static RequestPanel instance;

        protected override void Awake()
        {
            if(instance != null)
            {
                Destroy(gameObject);
                return;
            }
            instance = this;
            base.Awake();
        }

        protected virtual void OnDestroy()
        {
            if(instance == this)
            {
                instance = null;
            }
        }

        /// <summary>
        /// Идентификатор вопроса
        /// </summary>
        string idCode;

        /// <summary>
        /// Объект, запрашивающий ответ на вопрос
        /// </summary>
        IRequestable obj;

        [SerializeField]
        /// <summary>
        /// Текст для вывода вопроса
        /// </summary>
        TextMeshProUGUI questionText = null;
        
        /// <summary>
        /// Получен ответ от игрока
        /// </summary>
        public void ButtonClick(bool val)
        {
            obj.GetConfirm(idCode, val);
            Toggle(false);
        }

        /// <summary>
        /// Показать вопрос с определенным идентификатором
        /// </summary>
        public static void Show(IRequestable obj, string code, string question)
        {
            if (instance == null) {
                obj.GetConfirm(code, true);
                return;
            }
            
            instance.idCode = code;
            instance.obj = obj;
            instance.questionText.text = question.Loc();
            instance.Toggle(true);
        }

        /// <summary>
        /// Показать вопрос
        /// </summary>
        public static void Show(IRequestable obj, string question)
        {
            Show(obj, null, question);
        }
    }
}
