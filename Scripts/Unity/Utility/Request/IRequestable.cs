﻿namespace Lib.Unity
{
    /// <summary>
    /// Интерфейс объекта, имеющего возможность использовать запросы игроку
    /// </summary>
    public interface IRequestable
    {
        void GetConfirm(string idCode, bool value);
    }
}
