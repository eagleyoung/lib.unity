﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Lib.Unity.Utility
{
    /// <summary>
    /// Объект для управления перемещением между сценами
    /// </summary>
    public class LoadHandler : Panel
    {
        static LoadHandler instance;
        protected override void Awake()
        {
            base.Awake();
            if (instance != null)
            {
                Destroy(gameObject);
                return;
            }
            instance = this;
            SceneManager.activeSceneChanged += SceneChanged;
            StateChanged.AddListener(OnStateChanged);
        }

        private void SceneChanged(UnityEngine.SceneManagement.Scene arg0, UnityEngine.SceneManagement.Scene arg1)
        {
            StartCoroutine(CloseRoute());
        }

        [SerializeField]
        /// <summary>
        /// Время до закрытия панели
        /// </summary>
        float timeBeforeClose = 0.5f;

        IEnumerator CloseRoute()
        {
            yield return new WaitForSecondsRealtime(timeBeforeClose);
            Toggle(false);
        }

        protected virtual void OnDestroy()
        {
            if(instance == this)
            {
                instance = null;
                SceneManager.activeSceneChanged -= SceneChanged;
                StateChanged.RemoveListener(OnStateChanged);
            }
        }

        /// <summary>
        /// Ключ сцены для загрузки
        /// </summary>
        static string sceneToLoad = null;

        /// <summary>
        /// Запросить загрузку сцены
        /// </summary>
        public static void RequestScene(string scene)
        {
#if DEBUG_BUILD
            Debug.Log($"[LoadHandler] Запрос сцены {scene}");
#endif
            if(instance != null)
            {
                sceneToLoad = scene;
                instance.Toggle(true);
            }            
        }

        /// <summary>
        /// Запросить выход
        /// </summary>
        public static void RequestQuit()
        {
#if DEBUG_BUILD
            Debug.Log($"[LoadHandler] Запрос выхода");
#endif
            if (instance != null)
            {
                sceneToLoad = null;
                instance.Toggle(true);
            }            
        }

        void OnStateChanged(bool state)
        {
            if (state)
            {
                if (sceneToLoad == null)
                {
                    Application.Quit();
                }
                else
                {
                    StartCoroutine(LoadRoute());
                }
            }
        }

        /// <summary>
        /// Загрузка сцены
        /// </summary>
        IEnumerator LoadRoute()
        {
            var op = SceneManager.LoadSceneAsync(sceneToLoad);

            while (!op.isDone)
            {
                yield return null;
            }
        }
    }
}
