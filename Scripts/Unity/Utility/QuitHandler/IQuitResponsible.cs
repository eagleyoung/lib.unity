﻿namespace Lib.Unity
{
    /// <summary>
    /// Интерфейс объекта, реагирующего на нажатие кнопки "Назад"
    /// </summary>
    public interface IQuitResponsible
    {
        void QuitPressed();
    }
}
