﻿using UnityEngine.Events;

namespace Lib.Unity.Utility
{
    /// <summary>
    /// Объект с возможностью отслеживания и рассылки сообщения о нажатии кнопки назад
    /// </summary>
    public class RequestSubscriber : BasicObject, IQuitResponsible
    {
        /// <summary>
        /// Событие при нажатии кнопки назад
        /// </summary>
        public UnityEvent Traced;

        public void QuitPressed()
        {
            Traced.Invoke();
        }

        protected virtual void Start()
        {
            QuitHandler.Register(this);
        }

        protected virtual void OnDestroy()
        {
            QuitHandler.Unregister(this);
        }
    }
}
