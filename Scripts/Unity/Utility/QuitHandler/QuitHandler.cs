﻿using System.Collections.Generic;
using UnityEngine;

namespace Lib.Unity
{
    /// <summary>
    /// Объект для управления нажатием кнопки "Назад"
    /// </summary>
    public class QuitHandler : BasicObject
    {
        /// <summary>
        /// Стэк зарегистрированных объектов
        /// </summary>
        List<IQuitResponsible> objects = new List<IQuitResponsible>();

        static QuitHandler instance;

        protected virtual void Awake()
        {
            if(instance != null)
            {
                Destroy(gameObject);
                return;
            }

            instance = this;
        }

        protected virtual void OnDestroy()
        {
            if(instance == this)
            {
                instance = null;
            }
        }

        /// <summary>
        /// Зарегистрировать объект
        /// </summary>
        public static void Register(IQuitResponsible obj)
        {
            if(instance != null)
            {
                instance.objects.Insert(0, obj);
            }            
        }      

        void Update()
        {
            if (Input.GetButtonDown("Cancel"))
            {
                objects[0].QuitPressed();
            }
        }

        /// <summary>
        /// Убрать объект из обработки
        /// </summary>
        public static void Unregister(IQuitResponsible obj)
        {
            if (instance != null && instance.objects.Contains(obj))
            {
                instance.objects.Remove(obj);
            }
        }
    }
}
