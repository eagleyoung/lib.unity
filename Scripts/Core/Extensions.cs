﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Lib
{
    public static class Extensions
    {
        /// <summary>
        /// Данные для перевода в римское исчисление
        /// </summary>
        static string[][] romanNumerals = new string[][]
            {
                new string[]{"", "i", "ii", "iii", "iv", "v", "vi", "vii", "viii", "ix"},
                new string[]{"", "x", "xx", "xxx", "xl", "l", "lx", "lxx", "lxxx", "xc"},
                new string[]{"", "c", "cc", "ccc", "cd", "d", "dc", "dcc", "dccc", "cm"},
                new string[]{"", "m", "mm", "mmm"}
            };

        /// <summary>
        /// Перевести целочисленное значение в римский вид
        /// </summary>
        public static string ToRoman(this int number)
        {
            var intArr = number.ToString().Reverse().ToArray();
            var len = intArr.Length;
            var romanNumeral = "";
            var i = len;

            while (i-- > 0)
            {
                romanNumeral += romanNumerals[i][int.Parse(intArr[i].ToString())];
            }

            return romanNumeral;
        }

        /// <summary>
        /// Список возможных разделителей
        /// </summary>
        static List<string> deleters = new List<string>() {
            "",
            "k",
            "M",
            "G",
            "T",
            "P",
            "E",
            "Z",
            "Y"
        };

        /// <summary>
        /// Показ переменной типа double в строковом представлении с разделителями
        /// </summary>
        public static string StrRep(this double current)
        {
            double curToShow = current;
            int deleterIndex = 0;
            while (Math.Abs(curToShow) >= 1000f)
            {
                curToShow = curToShow / 1000f;
                deleterIndex++;
            }
            if (deleters.Count <= deleterIndex)
            {
#if DEBUG_BUILD
                Debug.LogWarning($"[Extensions] Не определен литерал для степени {deleterIndex}");
#endif
                deleterIndex = 0;
                curToShow = current;
            }
            if (deleterIndex > 0 && Math.Abs(curToShow) < 10f)
            {
                return curToShow.ToString("0.00") + deleters[deleterIndex];
            }
            else if (deleterIndex > 0 && Math.Abs(curToShow) < 100f)
            {
                return curToShow.ToString("0.0") + deleters[deleterIndex];
            }
            else if (Math.Abs(curToShow) < 1f && curToShow > double.Epsilon)
            {
                // Определяем минимальное количество нулей для показа
                int zer = 1;
                bool isNegative = curToShow < 0f;
                double tmp = Math.Abs(curToShow);
                while (tmp < 1f)
                {
                    zer++;
                    tmp = tmp * 10f;
                }
                return curToShow.ToString((isNegative ? "-" : "") + "0." + new string('0', zer));
            }
            else
            {
                return curToShow.ToString("0") + deleters[deleterIndex];
            }
        }
    }
}
