﻿using System;

namespace Lib
{
    /// <summary>
    /// Расширения для сравнения
    /// </summary>
    public static class Equality
    {
        /// <summary>
        /// Проверка на равенство double
        /// </summary>
        public static bool Equal(this double a, double b, double epsilon = 1E-15)
        {
            double absA = Math.Abs(a);
            double absB = Math.Abs(b);
            double diff = Math.Abs(a - b);

            if (a == b)
            {
                return true;
            }
            else if (a == 0 || b == 0 || diff < double.Epsilon)
            {
                return diff < epsilon;
            }
            else
            {
                return diff / (absA + absB) < epsilon;
            }
        }

        /// <summary>
        /// Проверка на равенство float
        /// </summary>
        public static bool Equal(this float a, float b, double epsilon = 1E-6)
        {
            float absA = Math.Abs(a);
            float absB = Math.Abs(b);
            float diff = Math.Abs(a - b);

            if (a == b)
            {
                return true;
            }
            else if (a == 0 || b == 0 || diff < float.Epsilon)
            {
                return diff < epsilon;
            }
            else
            {
                return diff / (absA + absB) < epsilon;
            }
        }
    }
}
