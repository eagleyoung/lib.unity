﻿namespace Lib.Data.Loc
{
    /// <summary>
    /// Дополнительные расширения классов
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Перевести ключ на текущий язык
        /// </summary>
        public static string Loc(this string key)
        {
            var param = LocManager.GetInstance().ByKey(key);
            if (param != null) return param.Current;
            return key;
        }
    }
}
