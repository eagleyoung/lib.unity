﻿using Lib.Data.Param;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.IO;

namespace Lib.Data.Loc
{
    /// <summary>
    /// Менеджер переводов
    /// </summary>
    public class LocManager : BaseDataManager<LocData>
    {
        static Lazy<LocManager> instance = new Lazy<LocManager>(()=> BaseDataManager.Create<LocManager,LocData>("Data/Loc"), true);
        public static LocManager GetInstance()
        {
            return instance.Value;
        }

        Lazy<ParamData> currentLang = new Lazy<ParamData>(() => ParamManager.GetInstance().ByKey("Utility.CurrentLang", DataInterface.Core.ParamTypes.String));
        [JsonIgnore]
        /// <summary>
        /// Текущий выбранный язык
        /// </summary>
        public ParamData CurrentLang
        {
            get
            {
                if(currentLang.Value.String == null)
                {
                    currentLang.Value.String = "en";
                }
                return currentLang.Value;
            }
        }

        protected override LocData GenerateNew(string key)
        {
            return new LocData() { Key = key, En = key, Ru = key };
        }
    }
}
