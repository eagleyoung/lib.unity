﻿using DataInterface.Core;
using Newtonsoft.Json;

namespace Lib.Data.Loc
{
    /// <summary>
    /// Перевод
    /// </summary>
    public class LocData : BaseData, ILocData
    {
        /// <summary>
        /// Английский перевод
        /// </summary>
        public string En { get; set; }

        /// <summary>
        /// Русский перевод
        /// </summary>
        public string Ru { get; set; }

        /// <summary>
        /// Текущий перевод
        /// </summary>
        [JsonIgnore]
        public string Current
        {
            get
            {
                switch (LocManager.GetInstance().CurrentLang.String)
                {
                    case "en":
                        return En;
                    case "ru":
                        return Ru;
                    default:
                        return Key;
                }
            }
        }
    }
}
