﻿using Lib.Data.Param;
using System;
using System.Collections.ObjectModel;

namespace Lib.Data
{
    /// <summary>
    /// Набор требований
    /// </summary>
    public class DataRequireCollection : Collection<DataRequire>
    {
        protected override void InsertItem(int index, DataRequire item)
        {
            base.InsertItem(index, item);
            var param = ParamManager.GetInstance().ByKey(item.ParamKey);
            if(param != null)
            {
                param.Changed += Check;
            }
        }

        /// <summary>
        /// Отвязаться от сообщений об изменении параметров
        /// </summary>
        void Unregister()
        {
            foreach (var require in this)
            {
                var param = ParamManager.GetInstance().ByKey(require.ParamKey);
                if (param != null)
                {
                    param.Changed -= Check;
                }
            }
        }

        ~DataRequireCollection()
        {
            Unregister();
        }

        private void Check()
        {
            StateChanged();
        }

        /// <summary>
        /// Набор требований выполнен?
        /// </summary>
        public bool Available
        {
            get
            {
                foreach (var require in this)
                {
                    var param = ParamManager.GetInstance().ByKey(require.ParamKey);
                    if (param == null) return false;

                    if (param < require.Value) return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Состояние набора изменилось
        /// </summary>
        public event Action StateChanged = delegate { };

        /// <summary>
        /// Произвести вычитание требований
        /// </summary>
        public void Proceed()
        {
            foreach (var require in this)
            {
                if (require.IsCost)
                {
                    var param = ParamManager.GetInstance().ByKey(require.ParamKey);
                    if (param == null) continue;

                    param -= require.Value;
                }                
            }
        }
    }
}
