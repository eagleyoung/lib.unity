﻿using DataInterface.Core;

namespace Lib.Data
{
    /// <summary>
    /// Отдельное требование
    /// </summary>
    public class DataRequire : IDataRequire
    {
        /// <summary>
        /// Списывать требование?
        /// </summary>
        public bool IsCost { get; set; }

        /// <summary>
        /// Ключ параметра для проверки
        /// </summary>
        public string ParamKey { get; set; }

        /// <summary>
        /// Требуемое значение
        /// </summary>
        public string Value { get; set; }
    }
}
