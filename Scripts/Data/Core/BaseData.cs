﻿using DataInterface.Core;

namespace Lib.Data
{
    /// <summary>
    /// Базовый объект данных
    /// </summary>
    public class BaseData : IBaseData
    {
        /// <summary>
        /// Ключ данных
        /// </summary>
        public string Key { get; set; }
    }
}
