﻿using System;

namespace Lib.Data
{
    /// <summary>
    /// Объект данных с возможностью рассылки информации об изменении состояния
    /// </summary>
    public class TraceableBaseData : BaseData
    {
        /// <summary>
        /// Состояние объекта изменилось
        /// </summary>
        public event Action StateChanged = delegate { };
        protected virtual void OnStateChanged()
        {
            StateChanged();
        }
    }
}
