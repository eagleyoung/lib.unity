﻿using Lib.Data.Param;
using System.Collections.ObjectModel;

namespace Lib.Data
{
    /// <summary>
    /// Коллекция изменений
    /// </summary>
    public class DataChangeCollection : Collection<DataChange>
    {
        /// <summary>
        /// Выполнить изменения
        /// </summary>
        public void Proceed()
        {
            foreach (var change in this)
            {
                var param = ParamManager.GetInstance().ByKey(change.ParamKey);
                if (param == null) continue;

                param += change.Total;
            }
        }
    }
}
