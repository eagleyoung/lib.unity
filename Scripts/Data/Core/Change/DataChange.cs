﻿using DataInterface.Core;
using Lib.Data.Param;

namespace Lib.Data
{
    /// <summary>
    /// Параметры изменения
    /// </summary>
    public class DataChange : IDataChange
    {
        /// <summary>
        /// Ключ параметра для изменения
        /// </summary>
        public string ParamKey { get; set; }

        /// <summary>
        /// Изменение
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Ключ параметра - множителя
        /// </summary>
        public string MultiplierKey { get; set; }

        /// <summary>
        /// Вычисленное значение изменения
        /// </summary>
        public string Total
        {
            get
            {
                var val = Value;
                if (MultiplierKey != null)
                {
                    var multiplier = ParamManager.GetInstance().ByKey(MultiplierKey);
                    if (multiplier != null)
                    {
                        val = multiplier.GetMultipliedValue(val);

                    }
                }

                return val;
            }
        }
    }
}
