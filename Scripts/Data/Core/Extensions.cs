﻿using DataInterface.Core;
using Lib.Data.Param;
using System.Collections.Generic;

namespace Lib.Data
{
    /// <summary>
    /// Расширения классов данных
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Получить требование по ключу
        /// </summary>
        public static DataRequire ByKey(this List<DataRequire> requires, string key)
        {
            foreach(var item in requires)
            {
                if (item.ParamKey == key) return item;
            }
            return null;
        }

        /// <summary>
        /// Получить текстовое представление требований
        /// </summary>
        public static string StrRep(this DataRequire require)
        {
            var data = ParamManager.GetInstance().ByKey(require.ParamKey);
            switch (data.ParamType)
            {
                case ParamTypes.Float:
                case ParamTypes.Integer:
                    var val = double.Parse(require.Value);
                    return val.StrRep();
                default:
                    return require.Value;
            }
        }

        /// <summary>
        /// Получить текстовое представление изменений
        /// </summary>
        public static string StrRep(this DataChange change)
        {
            var data = ParamManager.GetInstance().ByKey(change.ParamKey);
            switch (data.ParamType)
            {
                case ParamTypes.Float:
                case ParamTypes.Integer:
                    var val = double.Parse(change.Total);
                    return $"{(val < 0 ? "" : "+")}{val.StrRep()}";
                default:
                    return change.Total;
            }
        }
    }
}
