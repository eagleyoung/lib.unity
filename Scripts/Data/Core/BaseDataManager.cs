﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace Lib.Data
{
    /// <summary>
    /// Базовый менеджер данных
    /// </summary>
    public class BaseDataManager<T>
        where T : BaseData, new()
    {
        /// <summary>
        /// Набор элементов менеджера по ключу
        /// </summary>
        [JsonProperty]
        protected List<T> items = new List<T>();

        /// <summary>
        /// Список элементов по ключу
        /// </summary>
        protected Dictionary<string, T> byKey = new Dictionary<string, T>();

        /// <summary>
        /// Загрузить данные менеджера
        /// </summary>
        public void LoadKeys()
        {
            byKey.Clear();
            foreach (var item in items)
            {
                byKey.Add(item.Key, item);
            }
        }

        /// <summary>
        /// Получить объект по ключу
        /// </summary>
        public T ByKey(string key)
        {
            if (key == null) return null;
            T output = null;
            if (!byKey.TryGetValue(key, out output))
            {
#if DEBUG_BUILD
                Debug.LogError($"[{GetType().Name}] Отсутствует ключ {key}");
#endif
                output = GenerateNew(key);
                if (output != null)
                {
                    byKey.Add(output.Key, output);
                }
            }
            return output;
        }

        /// <summary>
        /// Создать новый элемент
        /// </summary>
        protected virtual T GenerateNew(string key)
        {
            return null;
        }

        /// <summary>
        /// Получить все элементы
        /// </summary>
        public IEnumerable<T> GetAll()
        {
            return byKey.Values;
        }
    }

    public static class BaseDataManager
    {
        /// <summary>
        /// Загрузить менеджер данных
        /// </summary>
        public static TManager Create<TManager, TItem>(string dataPath)
            where TManager : BaseDataManager<TItem>, new()
            where TItem : BaseData, new()
        {
            var initialState = Resources.Load<TextAsset>(dataPath);
            if (initialState != null)
            {
                var initialStateString = initialState.bytes.Decrypt();
#if DEBUG_BUILD
                Debug.Log($"[{dataPath}] Initial ({initialStateString})");
#endif
                var mgr = JsonConvert.DeserializeObject<TManager>(initialStateString);
                mgr.LoadKeys();
                return mgr;
            }
            else
            {
                return new TManager();
            }
        }
    }
}
