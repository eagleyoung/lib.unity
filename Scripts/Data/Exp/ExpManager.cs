﻿using Lib.Data.Param;
using System;

namespace Lib.Data.Exp
{
    /// <summary>
    /// Менеджер опыта
    /// </summary>
    public class ExpManager : BaseDataManager<ExpData>
    {
        static Lazy<ExpManager> instance = new Lazy<ExpManager>(()=> BaseDataManager.Create<ExpManager, ExpData>("Data/Exp"), true);
        public static ExpManager GetInstance()
        {
            return instance.Value;
        }
        
        Lazy<ParamData> exp = new Lazy<ParamData>(()=> {
            var param = ParamManager.GetInstance().ByKey("Utility.Exp", DataInterface.Core.ParamTypes.Float);
            param.Changed += OnExpGained;
            return param;
        });
        /// <summary>
        /// Количество опыта
        /// </summary>
        public double Exp
        {
            get
            {
                return exp.Value.Double;
            }
            set
            {
                if(value > exp.Value.Double)
                {
                    exp.Value.Double = value;
                    CheckLevel();
                }
            }
        }

        /// <summary>
        /// Событие при изменении количества опыта
        /// </summary>
        public static event Action ExpGained = delegate { };
        static void OnExpGained()
        {
            ExpGained?.Invoke();
        }
        
        /// <summary>
        /// Проверить получение уровня
        /// </summary>
        void CheckLevel()
        {
            var next = Next;
            if(next != null && exp.Value.Double >= next.Value)
            {
                expLevel.Value.Int++;
            }
        }

        /// <summary>
        /// Уровень игрока был изменен
        /// </summary>
        public static event Action ExpLevelChanged = delegate { };
        static void OnExpLevelChanged()
        {
            ExpLevelChanged?.Invoke();
        }

        Lazy<ParamData> expLevel = new Lazy<ParamData>(() => {
            var param = ParamManager.GetInstance().ByKey("Utility.ExpLevel", DataInterface.Core.ParamTypes.Integer);
            param.Changed += OnExpLevelChanged;
            return param;
        });

        /// <summary>
        /// Текущий уровень опыта
        /// </summary>
        public ExpData Current
        {
            get
            {
                var key = expLevel.Value;
                return ByKey((key.Int - 1).ToString());
            }
        }

        /// <summary>
        /// Следующий уровень опыта
        /// </summary>
        public ExpData Next
        {
            get
            {
                var key = expLevel.Value;
                return ByKey(key.Int.ToString());
            }
        }
    }
}
