﻿using DataInterface.Core;
using System.Collections.Generic;

namespace Lib.Data.Exp
{
    /// <summary>
    /// Уровень опыта
    /// </summary>
    public class ExpData : BaseData, IExpData
    {
        /// <summary>
        /// Значение переменной, при котором производится получение уровня
        /// </summary>
        public double Value { get; set; }

        /// <summary>
        /// Список изменений
        /// </summary>
        public DataChangeCollection Changes { get; set; } = new DataChangeCollection();
        IEnumerable<IDataChange> IExpData.Changes => Changes;
    }
}
