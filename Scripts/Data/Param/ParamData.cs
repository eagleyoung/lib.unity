﻿using DataInterface.Core;
using Newtonsoft.Json;
using System;
using UnityEngine;

namespace Lib.Data.Param
{
    /// <summary>
    /// Сохраняемые данные
    /// </summary>
    public partial class ParamData : BaseData, IParamData
    {
        public ParamData() { }
        public ParamData(string key)
        {
            Key = key;
        }

        /// <summary>
        /// Данные были изменены
        /// </summary>
        public event Action Changed = delegate { };
        void OnChanged()
        {
#if DEBUG_BUILD
            Debug.Log($"[ParamData] {Key} -> {Inner}");
#endif
            Changed();
        }

        /// <summary>
        /// Внутреннее представление данных
        /// </summary>
        public string Inner { get; set; }

        [JsonIgnore]
        /// <summary>
        /// Строковое значение
        /// </summary>
        public string String {
            get
            {
                return Inner;
            }
            set
            {
                Inner = value;
                OnChanged();
            }
        }

        /// <summary>
        /// Параметр должен быть сброшен при сбросе всех данных?
        /// </summary>
        public bool MustDrop { get; set; }
        
        /// <summary>
        /// Текущий тип переменной
        /// </summary>
        public ParamTypes ParamType { get; set; }

        [JsonIgnore]
        /// <summary>
        /// Булевое значение
        /// </summary>
        public bool Bool
        {
            get
            {
                bool output = default(bool);
                if(Inner != null && !bool.TryParse(Inner, out output))
                {
#if DEBUG_BUILD
                    Debug.LogError($"[ParamData:{Key}] Ошибка при конвертации {Inner} в bool");
#endif
                }
                return output;
            }
            set
            {
                Inner = value.ToString();
                OnChanged();
            }
        }

        [JsonIgnore]
        /// <summary>
        /// Децимальное значение
        /// </summary>
        public decimal Decimal
        {
            get
            {
                decimal output = default(decimal);
                if(Inner != null && !decimal.TryParse(Inner, out output))
                {
#if DEBUG_BUILD
                    Debug.LogError($"[ParamData:{Key}] Ошибка при конвертации {Inner} в decimal");
#endif
                }
                return output;
            }
            set
            {
                Inner = value.ToString();
                OnChanged();
            }
        }

        [JsonIgnore]
        /// <summary>
        /// Значение с плавающей запятой
        /// </summary>
        public float Float
        {
            get
            {
                float output = default(float);
                if(Inner != null && !float.TryParse(Inner, out output))
                {
#if DEBUG_BUILD
                    Debug.LogError($"[ParamData:{Key}] Ошибка при конвертации {Inner} в float");
#endif
                }
                return output;
            }
            set
            {
                Inner = value.ToString();
                OnChanged();
            }
        }

        /// <summary>
        /// Значение с плавающей запятой 64bit
        /// </summary>
        [JsonIgnore]        
        public double Double
        {
            get
            {
                double output = default(double);
                if(Inner != null && !double.TryParse(Inner, out output))
                {
#if DEBUG_BUILD
                    Debug.LogError($"[ParamData:{Key}] Ошибка при конвертации {Inner} в double");
#endif
                }
                return output;
            }
            set
            {
                Inner = value.ToString();
                OnChanged();
            }
        }

        /// <summary>
        /// Целочисленное значение
        /// </summary>
        [JsonIgnore]
        public int Int
        {
            get
            {
                int output = default(int);
                if(Inner != null && !int.TryParse(Inner, out output))
                {
#if DEBUG_BUILD
                    Debug.LogError($"[ParamData:{Key}] Ошибка при конвертации {Inner} в int");
#endif
                }
                return output;
            }
            set
            {
                Inner = value.ToString();
                OnChanged();
            }
        }

        /// <summary>
        /// Целочисленное значение 64bit
        /// </summary>
        [JsonIgnore]
        public long Long
        {
            get
            {
                long output = default(long);
                if (Inner != null && !long.TryParse(Inner, out output))
                {
#if DEBUG_BUILD
                    Debug.LogError($"[ParamData:{Key}] Ошибка при конвертации {Inner} в long");
#endif
                }
                return output;
            }
            set
            {
                Inner = value.ToString();
                OnChanged();
            }
        }
    }
}
