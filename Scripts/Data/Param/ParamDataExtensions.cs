﻿using DataInterface.Core;
using System;
using UnityEngine;

namespace Lib.Data.Param
{
    /// <summary>
    /// Дополнительные расширения класса
    /// </summary>
    public partial class ParamData
    {
        /// <summary>
        /// Получить помноженное значение в строковом виде без изменения переменной
        /// </summary>
        public string GetMultipliedValue(string value)
        {
            switch (ParamType)
            {
                case ParamTypes.Bool:
#if DEBUG_BUILD
                    Debug.LogError($"[ParamData:{Key}] Попытка bool умножения {Inner} * {value}");
#endif
                    break;
                case ParamTypes.Float:
                case ParamTypes.Integer:
                    double doubleValue = default(double);
                    if (double.TryParse(value, out doubleValue))
                    {
                        var val = Double * doubleValue;

                        if(ParamType == ParamTypes.Integer)
                        {
                            val = Math.Round(val);
                        }

                        return val.ToString();
                    }
                    else
                    {
#if DEBUG_BUILD
                        Debug.LogError($"[ParamData:{Key}] Ошибка при float умножении {Inner} * {value}");
#endif
                    }
                    break;
                case ParamTypes.String:
#if DEBUG_BUILD
                    Debug.LogError($"[ParamData:{Key}] Попытка string умножения {Inner} * {value}");
#endif
                    break;
                case ParamTypes.Undefined:
#if DEBUG_BUILD
                    Debug.LogError($"[ParamData] Не установлен тип уможения {Inner} * {value}");
#endif
                    break;
            }

            return null;
        }
    }
}
