﻿using DataInterface.Core;
using Newtonsoft.Json;
using System;
using System.Linq;
using UnityEngine;

namespace Lib.Data.Param
{
    /// <summary>
    /// Менеджер сохраняемых данных
    /// </summary>
    public class ParamManager : BaseDataManager<ParamData>
    {
        static Lazy<ParamManager> instance = new Lazy<ParamManager>(()=> {
            var inst = BaseDataManager.Create<ParamManager, ParamData>("Data/Param");

#if !DROPDATA
            string savedState = null;
            try
            {
                savedState = Convert.FromBase64String(PlayerPrefs.GetString("Param")).Decrypt();
            }
            catch { }            

            if (!string.IsNullOrEmpty(savedState))
            {
#if DEBUG_BUILD
                Debug.Log($"[ParamManager] Load ({savedState})");
#endif

                try
                {
                    var saved = JsonConvert.DeserializeObject<ParamManager>(savedState);
                    saved.LoadKeys();
                    inst.Load(saved);
                }catch(Exception ex)
                {
                    Debug.LogException(ex);
                }                
            }
#endif

            return inst;

        }, true);
        public static ParamManager GetInstance()
        {
            return instance.Value;
        }

        /// <summary>
        /// Загрузить сохраненные данные менеджера
        /// </summary>
        void Load(ParamManager savedState)
        {
            foreach (var pair in byKey)
            {
                var item = savedState.ByKey(pair.Key);
                if (item != null)
                {
                    pair.Value.String = item.String;
                }
            }

            foreach (var pair in savedState.byKey)
            {
                if (!byKey.ContainsKey(pair.Key))
                {
                    byKey.Add(pair.Key, pair.Value);
                }
            }
        }

        /// <summary>
        /// Сохранить текущее состояние данных
        /// </summary>
        public void Save()
        {
            items.Clear();
            foreach (var item in byKey.Values)
            {
                items.Add(item);
            }
            var serialized = JsonConvert.SerializeObject(this).Encrypt();

#if DEBUG_BUILD
            Debug.Log($"[ParamManager] Save ({serialized})");
#endif

            PlayerPrefs.SetString("Param", Convert.ToBase64String(serialized));
            PlayerPrefs.Save();
        }

        /// <summary>
        /// Сбросить данные игры
        /// </summary>
        public void Drop()
        {
#if DEBUG_BUILD
            Debug.Log($"[ParamManager] Drop");
#endif

            var toLoad = BaseDataManager.Create<ParamManager, ParamData>("Data/Param");

            var pairs = byKey.ToList();
            foreach (var pair in pairs)
            {
                if (pair.Value.MustDrop)
                {
                    ParamData item = null;
                    if (toLoad.byKey.TryGetValue(pair.Key, out item))
                    {
                        pair.Value.String = item.String;
                    }
                    else
                    {
                        pair.Value.String = null;
                    }
                }
            }
        }

        public ParamData ByKey(string key, ParamTypes type)
        {
            var output = ByKey(key);
            if(output.ParamType == ParamTypes.Undefined)
            {
                output.ParamType = type;
            }

#if DEBUG_BUILD
            if(type != ParamTypes.Undefined && type != output.ParamType)
            {
                Debug.LogError($"[ParamManager] Ошибка при использовании типа переменной {key}");
            }
#endif

            return output;
        }

        protected override ParamData GenerateNew(string key)
        {
            return new ParamData(key) { MustDrop = true };
        }
    }
}
