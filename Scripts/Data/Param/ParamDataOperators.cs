﻿using DataInterface.Core;
using UnityEngine;

namespace Lib.Data.Param
{
    public partial class ParamData
    {
        public static bool operator >= (ParamData param, string value)
        {
            switch (param.ParamType)
            {
                case ParamTypes.Bool:
                    bool boolValue = default(bool);
                    if (bool.TryParse(value, out boolValue))
                    {
                        return param.Bool == boolValue;
                    }
                    else
                    {
#if DEBUG_BUILD
                        Debug.LogError($"[ParamData:{param.Key}] Ошибка при bool сравнении {param.Inner} >= {value}");
#endif
                    }
                    break;
                case ParamTypes.Float:
                    double doubleValue = default(double);
                    if (double.TryParse(value, out doubleValue))
                    {
                        return param.Double >= doubleValue;
                    }
                    else
                    {
#if DEBUG_BUILD
                        Debug.LogError($"[ParamData:{param.Key}] Ошибка при float сравнении {param.Inner} >= {value}");
#endif
                    }
                    break;
                case ParamTypes.Integer:
                    long longValue = default(long);
                    if (long.TryParse(value, out longValue))
                    {
                        return param.Long >= longValue;
                    }
                    else
                    {
#if DEBUG_BUILD
                        Debug.LogError($"[ParamData:{param.Key}] Ошибка при integer сравнении {param.Inner} >= {value}");
#endif
                    }
                    break;
                case ParamTypes.String:
                    return param.String == value;
                case ParamTypes.Undefined:
#if DEBUG_BUILD
                    Debug.LogError($"[ParamData:{param.Key}] Не установлен тип сравнения {param.Inner} >= {value}");
#endif
                    break;
            }

            return false;
        }

        public static bool operator <=(ParamData param, string value)
        {
            switch (param.ParamType)
            {
                case ParamTypes.Bool:
                    bool boolValue = default(bool);
                    if (bool.TryParse(value, out boolValue))
                    {
                        if (param.Bool == boolValue) return true;
                        if (param.Bool && !boolValue) return true;
                    }
                    else
                    {
#if DEBUG_BUILD
                        Debug.LogError($"[ParamData:{param.Key}] Ошибка при bool сравнении {param.Inner} <= {value}");
#endif
                    }
                    break;
                case ParamTypes.Float:
                    double doubleValue = default(double);
                    if (double.TryParse(value, out doubleValue))
                    {
                        return param.Double <= doubleValue;
                    }
                    else
                    {
#if DEBUG_BUILD
                        Debug.LogError($"[ParamData:{param.Key}] Ошибка при float сравнении {param.Inner} <= {value}");
#endif
                    }
                    break;
                case ParamTypes.Integer:
                    long longValue = default(long);
                    if (long.TryParse(value, out longValue))
                    {
                        return param.Long <= longValue;
                    }
                    else
                    {
#if DEBUG_BUILD
                        Debug.LogError($"[ParamData:{param.Key}] Ошибка при integer сравнении {param.Inner} <= {value}");
#endif
                    }
                    break;
                case ParamTypes.String:
                    return param.String == value;
                case ParamTypes.Undefined:
#if DEBUG_BUILD
                    Debug.LogError($"[ParamData:{param.Key}] Не установлен тип сравнения {param.Inner} <= {value}");
#endif
                    break;
            }

            return false;
        }

        public static ParamData operator -(ParamData param, string value)
        {
            switch (param.ParamType)
            {
                case ParamTypes.Bool:
#if DEBUG_BUILD
                    Debug.LogError($"[ParamData:{param.Key}] Попытка bool вычитания {param.Inner} - {value}");
#endif
                    break;
                case ParamTypes.Float:
                    double doubleValue = default(double);
                    if (double.TryParse(value, out doubleValue))
                    {
                        param.Double -= doubleValue;
                    }
                    else
                    {
#if DEBUG_BUILD
                        Debug.LogError($"[ParamData:{param.Key}] Ошибка при float вычитании {param.Inner} - {value}");
#endif
                    }
                    break;
                case ParamTypes.Integer:
                    long longValue = default(long);
                    if (long.TryParse(value, out longValue))
                    {
                        param.Long -= longValue;
                    }
                    else
                    {
#if DEBUG_BUILD
                        Debug.LogError($"[ParamData:{param.Key}] Ошибка при integer вычитании {param.Inner} - {value}");
#endif
                    }
                    break;
                case ParamTypes.String:
#if DEBUG_BUILD
                    Debug.LogError($"[ParamData:{param.Key}] Попытка string вычитания {param.Inner} - {value}");
#endif
                    break;
                case ParamTypes.Undefined:
#if DEBUG_BUILD
                    Debug.LogError($"[ParamData:{param.Key}] Не установлен тип вычитания {param.Inner} - {value}");
#endif
                    break;
            }

            return param;
        }

        public static ParamData operator +(ParamData param, string value)
        {
            switch (param.ParamType)
            {
                case ParamTypes.Bool:
                    bool boolValue = default(bool);
                    if(bool.TryParse(value, out boolValue))
                    {
                        param.Bool = boolValue;
                    }
                    else
                    {
#if DEBUG_BUILD
                        Debug.LogError($"[ParamData:{param.Key}] Ошибка bool сложении {param.Inner} + {value}");
#endif
                    }
                    break;
                case ParamTypes.Float:
                    double doubleValue = default(double);
                    if (double.TryParse(value, out doubleValue))
                    {
                        param.Double += doubleValue;
                    }
                    else
                    {
#if DEBUG_BUILD
                        Debug.LogError($"[ParamData:{param.Key}] Ошибка при float сложении {param.Inner} + {value}");
#endif
                    }
                    break;
                case ParamTypes.Integer:
                    long longValue = default(long);
                    if (long.TryParse(value, out longValue))
                    {
                        param.Long += longValue;
                    }
                    else
                    {
#if DEBUG_BUILD
                        Debug.LogError($"[ParamData:{param.Key}] Ошибка при integer сложении {param.Inner} + {value}");
#endif
                    }
                    break;
                case ParamTypes.String:
                    param.String = value;
                    break;
                case ParamTypes.Undefined:
#if DEBUG_BUILD
                    Debug.LogError($"[ParamData:{param.Key}] Не установлен тип сложения {param.Inner} + {value}");
#endif
                    break;
            }

            return param;
        }

        public static bool operator >(ParamData param, string value)
        {
            return !(param <= value);
        }

        public static bool operator <(ParamData param, string value)
        {
            return !(param >= value);
        }
    }
}
