﻿using DataInterface.Core;
using Lib.Data.Param;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Lib.Data.Level
{
    /// <summary>
    /// Уровень игры
    /// </summary>
    public class LevelData : TraceableBaseData, ILevelData
    {
        /// <summary>
        /// Требования для перехода на уровень
        /// </summary>
        public DataRequireCollection Requires { get; set; } = new DataRequireCollection();
        IEnumerable<IDataRequire> ILevelData.Requires => Requires;

        public LevelData()
        {
            Requires.StateChanged += OnStateChanged;
        }

        ~LevelData()
        {
            Requires.StateChanged -= OnStateChanged;
        }

        [JsonIgnore]
        /// <summary>
        /// Уровень куплен?
        /// </summary>
        public bool Bought
        {
            get
            {
                return ParamManager.GetInstance().ByKey($"Level.{Key}.Bought", ParamTypes.Bool).Bool;
            }
            set
            {
                ParamManager.GetInstance().ByKey($"Level.{Key}.Bought", ParamTypes.Bool).Bool = value;
                OnStateChanged();
            }
        }
    }
}
