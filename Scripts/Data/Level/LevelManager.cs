﻿using Lib.Data.Param;
using System;

namespace Lib.Data.Level
{
    /// <summary>
    /// Менеджер уровней игры
    /// </summary>
    public class LevelManager : BaseDataManager<LevelData>
    {
        static Lazy<LevelManager> instance = new Lazy<LevelManager>(()=> BaseDataManager.Create<LevelManager,LevelData>("Data/Level"), true);
        public static LevelManager GetInstance()
        {
            return instance.Value;
        }

        Lazy<ParamData> currentLevelKey = new Lazy<ParamData>(()=> ParamManager.GetInstance().ByKey("Utility.CurrentLevel", DataInterface.Core.ParamTypes.Integer));
        /// <summary>
        /// Текущий уровень
        /// </summary>
        public LevelData CurrentLevel
        {
            get
            {
                var param = currentLevelKey.Value;
                return GetInstance().ByKey(param.Int.ToString());
            }
            set
            {
                currentLevelKey.Value.String = value.Key;
                OnLevelChanged();
            }
        }

        /// <summary>
        /// Событие при изменении уровня
        /// </summary>
        public static event Action LevelChanged = delegate { };
        static void OnLevelChanged()
        {
            LevelChanged?.Invoke();
        }
    }
}
