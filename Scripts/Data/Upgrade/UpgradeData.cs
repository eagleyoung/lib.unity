﻿using DataInterface.Core;
using Lib.Data.Param;
using System.Collections.Generic;

namespace Lib.Data.Upgrade
{
    /// <summary>
    /// Улучшение
    /// </summary>
    public class UpgradeData : TraceableBaseData, IUpgradeData
    {
        /// <summary>
        /// Требования для покупки улучшения
        /// </summary>
        public DataRequireCollection Requires { get; set; } = new DataRequireCollection();
        IEnumerable<IDataRequire> IUpgradeData.Requires => Requires;

        /// <summary>
        /// Изменения при покупке улучшения
        /// </summary>
        public DataChangeCollection Changes { get; set; } = new DataChangeCollection();
        IEnumerable<IDataChange> IUpgradeData.Changes => Changes;
        
        public UpgradeData()
        {
            Requires.StateChanged += OnStateChanged;
        }

        ~UpgradeData()
        {
            Requires.StateChanged -= OnStateChanged;
        }

        /// <summary>
        /// Улучшение куплено?
        /// </summary>
        public bool Bought
        {
            get
            {
                return ParamManager.GetInstance().ByKey($"Upgrade.{Key}.Bought", ParamTypes.Bool).Bool;
            }
            set
            {
                ParamManager.GetInstance().ByKey($"Upgrade.{Key}.Bought", ParamTypes.Bool).Bool = value;
                OnStateChanged();
            }
        }

        /// <summary>
        /// Возможно повторно покупать улучшение?
        /// </summary>
        public bool Repeatable { get; set; }
    }
}
