﻿using System;

namespace Lib.Data.Upgrade
{
    /// <summary>
    /// Менеджер улучшений
    /// </summary>
    public class UpgradeManager : BaseDataManager<UpgradeData>
    {
        static Lazy<UpgradeManager> instance = new Lazy<UpgradeManager>(()=> BaseDataManager.Create<UpgradeManager,UpgradeData>("Data/Upgrade"), true);
        public static UpgradeManager GetInstance()
        {
            return instance.Value;
        }
    }
}
